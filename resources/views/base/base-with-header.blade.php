<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

    <!-- Taost Css -->
    <link rel="stylesheet" href="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.css') }}">

    @yield('css')

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Jequry Taost Js -->
    <script src="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>
    @yield('top-js')
</head>

<body class="theme-green">
    @include('shared.widgets.loader', ['color' => 'green'])
    {{-- @include('shared.menu.default') --}}

    @yield('content')
    <script>
        @if (session('success'))
            $.toast({
            heading: 'Succès',
            text: "{{ session('success') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            bgColor: "#4CAF50",
            icon: 'success'
            })
        @endif

        @if (session('error'))
            $.toast({
            heading: 'Erreur',
            text: "{{ session('error') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'error'
            })
        @endif


        @if (session('warning'))
            $.toast({
            heading: 'Attention',
            text: "{{ session('warning') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'warning'
            })
        @endif
    </script>
    @yield('js')
    <script>
        function Previous() {
            window.history.back()
        }
    </script>
</body>

</html>
