<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name') }} - @yield('title')</title>
    <!-- CSS-->
    @yield('css')
    @yield('top-js')
</head>

<body class="theme-green" style="background-color: white !important;">
    @include('shared.widgets.loader', ['color' => 'indigo'])
    @yield('content')
    @yield('js')
    <script>
        function Previous() {
            window.history.back()
        }
    </script>
</body>

</html>
