@extends('base.base-without-header-login')

@section('title', 'Se connecter')




@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

    <!-- TOAST Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.css') }}">

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/node-waves/waves.css') }}">

    <!-- Animation Css -->
    <link rel="stylesheet" href="{{ asset('plugins/animate-css/animate.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <style>
        .login-page {
            background-color: #3F51B5 !important;
        }

        .login-page .login-box .align-right>a:nth-child(1) {
            color: #3F51B5 !important;
        }

    </style>

@endsection

@section('content')
    @include('shared.widgets.loader', ['color' => 'indigo'])
    <div class="login-box" style="background-color: #fff !important;">
        <div class="logo"
            style="display: flex !important; justify-content: center !important; padding-top: 10px !important; padding-bottom: 10px; margin-bottom: 10px !important; box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2); z-index: 400">
            {{-- <img src="{{ asset('images/only-logo.png') }}" style="width: 75px !important; height: 75px !important;"> --}}
            <img src=" {{ asset('images/logo-mini.png') }}">
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_in" method="post" action="{{ route('auth.login.store') }}">
                    @csrf
                    <div class="msg">Connectez-vous pour commencer votre
                        session
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username"
                                placeholder="Entrez votre nom d'utilisateur" required autofocus>
                        </div>
                    </div>

                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password"
                                placeholder="Entrez votre mot de passe" required>
                        </div>
                    </div>

                    <div class="form-group mb-1">
                        <div class="g-recaptcha d-flex justify-content-center"
                            data-sitekey="{{ env('GOOGLE_CAPTCHA_KEY') }}" data-callback="recaptchaDataCallbackLogin"
                            data-expired-callback="recaptchaExpireCallbackLogin">
                        </div>
                        {{-- <input type="hidden" name="grecaptcha"
                            id="hiddenRecaptchaLogin"> --}}
                        <div id="hiddenRecaptchaLogin_error"></div>
                        @if ($errors->any('grecaptcha'))
                            <span class="text-danger">{{ $errors->first('grecaptcha') }}</span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-xs-6 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-green">
                            <label for="rememberme">Se souvenir</label>
                        </div>
                        <div class="col-xs-6">
                            <button class="btn btn-block bg-green waves-effect" type="submit">CONNECTEZ-VOUS</button>
                        </div>
                    </div>

                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6 align-right">
                            <a href="">Mot de passe oublié ?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Jequry Taost Js -->
    <script src="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Google recaptcha Plugin Js -->
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/examples/sign-in.js') }}"></script>
    <script src="{{ asset('customer/js/login.js') }}"></script>

    <script>
        @if (session('success'))
            $.toast({
            heading: 'Succès',
            text: "{{ session('success') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            bgColor: "#4CAF50",
            icon: 'success'
            })
        @endif
        // Tag


        @if (session('error'))
            $.toast({
            heading: 'Erreur',
            text: "{{ session('error') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'error'
            })
        @endif


        @if (session('warning'))
            $.toast({
            heading: 'Attention',
            text: "{{ session('warning') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'warning'
            })
        @endif
    </script>
@endsection
