@extends('base.base-without-header-login')

@section('title', 'Mot de passe oublié')




@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

    <!-- TOAST Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.css') }}">

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/node-waves/waves.css') }}">

    <!-- Animation Css -->
    <link rel="stylesheet" href="{{ asset('plugins/animate-css/animate.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <style>
        .login-page {
            background-color: #3F51B5 !important;
        }

        .login-page .login-box .align-right>a:nth-child(1) {
            color: #3F51B5 !important;
        }

    </style>

@endsection

@section('content')
    @include('shared.widgets.loader', ['color' => 'indigo'])
    <div class="login-box bg-indigo">
        <div class="logo">
            <a href="javascript:void(0);" class="font-15">{{ config('app.name') }}</a>
        </div>
        <div class="card">
            <div class="body">
                <form id="forgot_password" method="POST" action="#">
                    {{ csrf_field() }}
                    <div class="msg">
                        Saisissez l'adresse e-mail associée au compte dont vous avez oublié le mot de passe. Nous vous
                        enverrons un e-mail avec un lien pour réinitialiser votre mot de passe.
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="forgot_password_email" placeholder="Email"
                                required autofocus>
                        </div>
                    </div>

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">RÉINITIALISER MOT LE DE
                        PASSE</button>

                    <div class="row m-t-20 m-b--5 align-center">
                        <a href="#">Se connecter</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Jequry Taost Js -->
    <script src="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Google recaptcha Plugin Js -->
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/examples/forgot-password.js') }}"></script>
    <script src="{{ asset('customer/js/login.js') }}"></script>

    <script>
        @if (session('success'))
            $.toast({
            heading: 'Succès',
            text: "{{ session('success') }}",
            hideAfter: 20000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            bgColor: "#4CAF50",
            icon: 'success'
            })
        @endif
        // Tag


        @if (session('error'))
            $.toast({
            heading: 'Erreur',
            text: "{{ session('error') }}",
            hideAfter: 20000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'error'
            })
        @endif


        @if (session('warning'))
            $.toast({
            heading: 'Attention',
            text: "{{ session('warning') }}",
            hideAfter: 20000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'warning'
            })
        @endif
    </script>
@endsection
