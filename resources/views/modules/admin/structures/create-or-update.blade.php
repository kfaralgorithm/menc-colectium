@extends('base.base-with-header')

@section('title', $is_creation ? 'Creation d\'un structure' : 'Mise à jour d\'un structure')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

        #child-closer {
            color: #000 !important
        }

        #child-closer:hover {
            color: #F44336 !important;
        }

    </style>

@endsection


@section('content')
    @include('shared.menu.overlay')

    @include('shared.menu.search')

    @include('shared.menu.navbar')

    @include('shared.menu.default', ['activa' => 'structures'])

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2 class="pull-left">
                    Structure
                    <small><u>Interface de gestion des structures</u></small>
                </h2>
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Accueil</a></li>
                    <li><a href="{{ route('admin.structures.index') }}">Structure</a></li>
                    <li class="active">
                        {{ $is_creation ? 'Creation' : 'Mise à jour' }}
                    </li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ $is_creation ? 'Creation d\'un structure' : 'Mise à jour d\'un structure' }}
                                @if (isset($structure) && !$is_creation)
                                    <span class="badge bg-blue">{{ $structure->name }}</span>
                                @endif
                            </h2>
                        </div>
                        <div class="body">
                            {{-- UPDATE NAV TAB HEADER --}}
                            @if (!$is_creation)
                                <div>
                                    <ul class="nav nav-tabs" role="tablist" id="create-or-update-structure-tab">
                                        <li role="presentation" class="active"><a href="#general"
                                                aria-controls="general" role="tab" data-toggle="tab">Généralité</a></li>
                                        <li role="presentation"><a href="#associated-programs"
                                                aria-controls="associated-programs" role="tab" data-toggle="tab">Programmes
                                                associés</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="general">
                            @endif
                            {{-- UPDATE NAV TAB HEADER END --}}

                            <form class="form-create-or-update-one"
                                action="{{ $is_creation ? route('admin.structures.store') : route('admin.structures.update', ['structure' => $structure->id]) }}"
                                method="POST">
                                @csrf
                                @if (!$is_creation)
                                    @method("PUT")
                                @endif
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-structure-name">Nom</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="create-or-update-structure-name"
                                                    class="form-control" placeholder="Entrez le nom de la structure"
                                                    name="create_or_update_structure_name"
                                                    value="{{ isset($structure) ? $structure->name : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-structure-acronym">Sigle</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="create-or-update-structure-acronym"
                                                    class="form-control" placeholder="Entrez le sigle de la structure"
                                                    name="create_or_update_structure_acronym"
                                                    value="{{ isset($structure) ? $structure->acronym : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-structure-password">Mot de passe</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">

                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="password" id="create-or-update-structure-password"
                                                    class="form-control" placeholder="Entrez le mot de passe associé."
                                                    name="create_or_update_structure_password"
                                                    value="{{ isset($structure) && isset($structure->user) ? $structure->user->username : '' }}">
                                            </div>
                                            <span class="input-group-addon">
                                                <i class="material-icons">visibility_off</i>
                                            </span>
                                        </div>

                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-structure-type">Type de structure</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick" data-live-search="true"
                                                title="Sélectionnez le type de structure"
                                                name="create_or_update_structure_type">
                                                <option value="" {{ $is_creation ? 'selected' : '' }}>Sélectionnez le
                                                    type
                                                    de structure</option>
                                                @foreach ($types->sortByDesc('name') as $type)
                                                    <option value="{{ $type->id }}"
                                                        {{ !$is_creation && (isset($structure) && isset($structure->type) && $structure->type->id == $type->id) ? 'selected' : '' }}>
                                                        {{ $type->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <input type="checkbox" id="create-or-update-structure-enable"
                                            class="filled-in chk-col-green" checked name="create_or_update_structure_enable"
                                            {{ $is_creation || (isset($structure) && $structure->is_enable == 1) ? 'checked' : '' }}>

                                        <label for="create-or-update-structure-enable">Activé</label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="pull-right m-r--10">
                                        <button type="submit"
                                            class="btn btn-primary m-t-15 waves-effect">{{ $is_creation ? 'Enregistrer' : 'Mettre à jour' }}</button>
                                    </div>
                                </div>
                            </form>

                            {{-- UPDATE NAV TAB SUB STRUCTURE PANEL AND FOOTER --}}
                            @if (!$is_creation)
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="associated-programs">
                            <div>
                                <strong class="font-15">Liste des programmes associés

                                    @if ($structure->programs->isNotEmpty())
                                        <span class="badge bg-blue">{{ $structure->programs->count() }}</span>
                                    @endif
                                </strong>
                                <div class="content m-t-5">
                                    @if ($structure->programs->isEmpty())
                                        <p class="font-italic font-15 m-l-45 col-blue-grey">00 Sous structures</p>
                                    @else
                                        @foreach ($structure->programs->sortByDesc('created_at') as $child)
                                            <span class="badge  bg-green"><span>{{ $child->name }}</span>
                                                <form
                                                    action="{{ route('admin.structures.programs.remove', ['structure' => $structure->id]) }}"
                                                    method="POST" class="pull-left m-r-5" style="display:inline !important">
                                                    @csrf
                                                    @method("DELETE")
                                                    <input type="hidden" name="programs_add_or_remove_from_structure"
                                                        value="{{ $child->id }}">
                                                    <button type="submit"
                                                        style="display: inline-block !important; background-color: transparent !important; outline: none; border: none;"
                                                        class="btn-link"><i class="material-icons font-10"
                                                            id="child-closer" style="cursor: pointer;">close</i></button>
                                                </form>
                                            </span>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @if (isset($possible_programs) && $possible_programs->isNotEmpty())
                                <div class="m-t-30">
                                    <strong class="font-15">Nouveau programme associé</strong>

                                    <form
                                        action="{{ route('admin.structures.programs.add', ['structure' => $structure->id]) }}"
                                        method="POST">
                                        @csrf
                                        <div class="row clearfix m-t-10">
                                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 form-control-label">
                                                <label for="create-or-update-structure-program-add">Programme <span
                                                        class="col-red">*</span></label>
                                            </div>

                                            <div class="col-lg-8 col-md-7 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <select class="form-control show-tick ms" data-live-search="true"
                                                        title="Sélectionnez le programme"
                                                        id="create-or-update-structure-program-add"
                                                        name="programs_add_or_remove_from_structure[]">
                                                        <option value="">Sélectionnez le programme</option>
                                                        @foreach ($possible_programs->sortByDesc('name') as $sub)
                                                            <option value="{{ $sub->id }}">
                                                                {{ $sub->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary waves-effect">Ajouter</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @endif

                {{-- UPDATE NAV TAB SUB STRUCTURE PANEL AND FOOTER END --}}

            </div>
        </div>
        </div>
        <!-- #END# Exportable Table -->
        </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>

    <script>
        $('#create-or-update-structure-tab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        // store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#create-or-update-structure-tab a[href="' + hash + '"]').tab('show');
    </script>
@endsection
