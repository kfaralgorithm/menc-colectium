@extends('base.base-with-header')

@section('title', $is_creation ? 'Creation d\'un programme' : 'Mise à jour d\'un programme')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

        #child-closer {
            color: #000 !important
        }

        #child-closer:hover {
            color: #F44336 !important;
        }

    </style>

@endsection


@section('content')
    @include('shared.menu.overlay')

    @include('shared.menu.search')

    @include('shared.menu.navbar')

    @include('shared.menu.default', ['activa' => 'programs'])

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2 class="pull-left">
                    PROGRAMME
                    <small><u>Interface de gestion des programmes</u></small>
                </h2>
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Accueil</a></li>
                    <li><a href="{{ route('admin.programs.index') }}">Programme</a></li>
                    <li class="active">
                        {{ $is_creation ? 'Creation' : 'Mise à jour' }}
                    </li>
                </ol>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ $is_creation ? 'Creation d\'un programme' : 'Mise à jour d\'un programme' }}
                                @if (isset($program) && !$is_creation)
                                    <span class="badge bg-blue">{{ $program->name }}</span>
                                @endif
                            </h2>
                        </div>
                        <div class="body">
                            {{-- UPDATE NAV TAB HEADER --}}
                            @if (!$is_creation)
                                <div>
                                    <ul class="nav nav-tabs" role="tablist" id="create-or-update-program-tab">
                                        <li role="presentation" class="active"><a href="#general"
                                                aria-controls="general" role="tab" data-toggle="tab">Généralité</a></li>
                                        <li role="presentation"><a href="#members" aria-controls="members" role="tab"
                                                data-toggle="tab">Structures intervenants</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="general">
                            @endif
                            {{-- UPDATE NAV TAB HEADER END --}}
                            <form class="form-horizontal"
                                action=" {{ $is_creation ? route('admin.programs.store') : route('admin.programs.update', ['program' => $program->id]) }}"
                                method="POST">
                                @csrf
                                @if (!$is_creation)
                                    @method("PUT")
                                @endif
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-program-name">Nom</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="create-or-update-program-name"
                                                    name="create_or_update_program_name" class="form-control"
                                                    placeholder="Entrez le nom du programme"
                                                    value="{{ isset($program) ? $program->name : '' }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-program-objective">Objectif</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="2" class="form-control no-resize auto-growth"
                                                    placeholder="Entrez l'objectif du programme"
                                                    id="create-or-update-program-objective"
                                                    name="create_or_update_program_objective">{{ isset($program) ? $program->objective : '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-program-description">Description</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="2" class="form-control no-resize auto-growth"
                                                    placeholder="Entrez la description du programme"
                                                    id="create-or-update-program-description"
                                                    name="create_or_update_program_description">{{ isset($program) ? $program->description : '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <input type="checkbox" id="create-or-update-program-enable"
                                            name="create_or_update_program_enable" class="filled-in chk-col-green"
                                            {{ $is_creation || (isset($program) && $program->is_enable == 1) ? 'checked' : '' }}>
                                        <label for="create-or-update-program-enable">Activé</label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="pull-right m-r--10">
                                        <button type="submit"
                                            class="btn btn-primary m-t-15 waves-effect">{{ $is_creation ? 'Enregistrer' : 'Mettre à jour' }}</button>
                                    </div>
                                </div>
                            </form>
                            {{-- UPDATE NAV TAB SUB PROGRAM PANEL AND FOOTER --}}
                            @if (!$is_creation)
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="members">
                            <div>
                                <strong class="font-15">Liste des structures intervenants

                                    @if ($program->structures->isNotEmpty())
                                        <span class="badge bg-blue">{{ $program->structures->count() }}</span>
                                    @endif
                                </strong>
                                <div class="content m-t-5">
                                    @if ($program->structures->isEmpty())
                                        <p class="font-italic font-15 m-l-45 col-blue-grey">00 Structure intervenant</p>
                                    @else
                                        @foreach ($program->structures->sortByDesc('created_at') as $structure)
                                            <span class="badge  bg-green"><span>{{ $structure->name }}</span>
                                                <form
                                                    action="{{ route('admin.programs.members.remove', ['program' => $program->id]) }}"
                                                    method="POST" class="pull-left m-r-5" style="display:inline !important">
                                                    @csrf
                                                    @method("DELETE")
                                                    <input type="hidden" name="members_add_or_remove_program"
                                                        value="{{ $structure->id }}">
                                                    <button type="submit"
                                                        style="display: inline-block !important; background-color: transparent !important; outline: none; border: none;"
                                                        class="btn-link"><i class="material-icons font-10"
                                                            id="child-closer" style="cursor: pointer;">close</i></button>
                                                </form>
                                            </span>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @if (isset($possible_members) && $possible_members->isNotEmpty())
                                <div class="m-t-30">
                                    <strong class="font-15">Nouvelle sous
                                        program</strong>

                                    <form action="{{ route('admin.programs.members.add', ['program' => $program->id]) }}"
                                        method="POST">
                                        @csrf
                                        <div class="row clearfix m-t-10">
                                            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 form-control-label">
                                                <label for="create-or-update-program-member">Sous
                                                    program</label>
                                            </div>

                                            <div class="col-lg-8 col-md-7 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <select class="form-control show-tick ms" data-live-search="true"
                                                        title="Sélectionnez la membre" id="create-or-update-program-member"
                                                        name="members_add_or_remove_program[]">
                                                        <option value="">Sélectionnez la membre</option>
                                                        @foreach ($possible_members->sortByDesc('name') as $sub)
                                                            <option value="{{ $sub->id }}">
                                                                {{ $sub->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                                <button type="submit" class="btn btn-primary waves-effect">Ajouter</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @endif

                {{-- UPDATE NAV TAB SUB PROGRAM PANEL AND FOOTER END --}}

            </div>
        </div>
        </div>
        <!-- #END# Exportable Table -->
        </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>


    <!-- Autosize Plugin Js -->
    <script src="{{ asset('plugins/autosize/autosize.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>

    <script>
        $('#create-or-update-program-tab a').click(function(e) {
            e.preventDefault();
            $(this).tab('show');
        });

        // store the currently selected tab in the hash value
        $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
            var id = $(e.target).attr("href").substr(1);
            window.location.hash = id;
        });

        // on load of the page: switch to the currently selected tab
        var hash = window.location.hash;
        $('#create-or-update-program-tab a[href="' + hash + '"]').tab('show');
    </script>
@endsection
