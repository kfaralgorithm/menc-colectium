@extends('base.base-with-header')

@section('title', $is_creation ? 'Creation d\'un unité' : 'Mise à jour d\'un unité')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

    </style>

@endsection


@section('content')
    @include('shared.menu.overlay')

    @include('shared.menu.search')

    @include('shared.menu.navbar')

    @include('shared.menu.default', ['activa' => 'units'])

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2 class="pull-left">
                    UNITÉ
                    <small><u>Interface de gestion des unités</u></small>
                </h2>
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Accueil</a></li>
                    <li><a href="{{ route('admin.units.index') }}">Unités</a></li>
                    <li class="active">
                        {{ $is_creation ? 'Creation' : 'Mise à jour' }}
                    </li>
                </ol>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{ $is_creation ? 'Creation d\'un unité' : 'Mise à jour d\'un unité' }}
                                @if (isset($unit) && !$is_creation)
                                    <span class="badge bg-blue">{{ $unit->name }}</span>
                                @endif
                            </h2>
                        </div>
                        <div class="body">

                            <form class="form-horizontal"
                                action="{{ $is_creation ? route('admin.units.store') : route('admin.units.update', ['unit' => $unit->id]) }}"
                                method="POST">
                                @csrf
                                @if (!$is_creation)
                                    @method("PUT")
                                @endif
                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-unit-name">Nom <span
                                                class="col-red">*</span></label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="create-or-update-unit-name"
                                                    name="create_or_update_unit_name" class="form-control"
                                                    placeholder="Entrez le nom de l'unité"
                                                    value="{{ isset($unit) ? $unit->name : '' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-unit-symbol">Symbole <span
                                                class="col-red">*</span></label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="create-or-update-unit-symbol"
                                                    name="create_or_update_unit_symbol" class="form-control"
                                                    placeholder="Entrez le symbole associé à l'unité"
                                                    value="{{ isset($unit) ? $unit->symbol : '' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-unit-description">Description</label>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth"
                                                    placeholder="Entrez la description de l'unité"
                                                    id="create-or-update-unit-description"
                                                    name="create_or_update_unit_description">{{ isset($unit) ? $unit->description : '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-offset-2 col-md-offset-2 col-sm-offset-4 col-xs-offset-5">
                                        <input type="checkbox" id="create-or-update-unit-enable"
                                            name="create_or_update_unit_enable" class="filled-in chk-col-green"
                                            {{ $is_creation || (isset($unit) && $unit->is_enable == 1) ? 'checked' : '' }}>
                                        <label for="create-or-update-unit-enable">Activé</label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="pull-right m-r--10">
                                        <button type="submit"
                                            class="btn btn-primary m-t-15 waves-effect">{{ $is_creation ? 'Enregistrer' : 'Mettre à jour' }}</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
        </div>
    </section>

@endsection


@section('js')

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Autosize Plugin Js -->
    <script src="{{ asset('plugins/autosize/autosize.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>
@endsection
