@extends('base.base-with-header')

@section('title', 'Indicateurs')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

    </style>

@endsection


@section('content')
    @include('shared.menu.overlay')

    @include('shared.menu.search')

    @include('shared.menu.navbar')

    @include('shared.menu.default', ['activa' => 'indicators'])

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2 class="pull-left">
                    INDICATEUR
                    <small><u>Interface de gestion des indicateurs</u></small>
                </h2>
                {{ session('success') }}
                <ol class="breadcrumb pull-right">
                    <li><a href="#">Accueil</a></li>
                    <li class="active">Indicateur</li>
                </ol>
            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Liste des indicateurs
                            </h2>
                            <a href="{{ route('admin.indicators.create') }}"
                                class="btn bg-green btn-circle waves-effect waves-cyan waves-circle waves-float pull-right"
                                style="margin-bottom: 10px;">
                                <i class="material-icons">add</i>
                            </a>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-sm table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nom</th>
                                            <th>Nbr Donnée</th>
                                            <th>Status</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Nom</th>
                                            <th>Nbr Donnée</th>
                                            <th>Status</th>
                                            <th class="align-center">Action</th>
                                        </tr>
                                    </tfoot>

                                    @php
                                        $counta = $indicators->count();
                                    @endphp

                                    <tbody>
                                        @foreach ($indicators->sortByDesc('created_at') as $indicator)
                                            <tr>
                                                <td>{{ $counta }}</td>
                                                <td>{{ $indicator->name }}</td>
                                                <td>{{ $indicator->data->count() }}</td>
                                                <td> <span
                                                        class="{{ $indicator->is_enable == 1 ? 'badge bg-green' : 'badge bg-red' }}">
                                                        {{ $indicator->is_enable == 1 ? 'Activé' : 'Désactivé' }}
                                                    </span>
                                                </td>
                                                <td class="align-center">
                                                    <a class="btn btn-primary"
                                                        href="{{ route('admin.indicators.edit', ['indicator' => $indicator->id]) }}"><i
                                                            class="material-icons">visibility</i></a>
                                                    <button class="btn btn-danger indicator-delete"><i
                                                            class="material-icons">delete</i></button>
                                                </td>
                                            </tr>

                                            @php
                                                $counta--;
                                            @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>
@endsection
