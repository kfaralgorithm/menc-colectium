@extends('base.base-without-header')

@section('title', 'Point focal - Liste des données dans Programme 01')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}">

    <!-- Bootstrap Select Css -->
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/node-waves/waves.css') }}">
    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.css') }}">

    <!-- Animation Css -->
    <link rel="stylesheet" href="{{ asset('plugins/animate-css/animate.css') }}">


    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}"
        rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link rel="stylesheet" href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}">

    <!-- Custom Css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

        #dropdownMenu1 {
            box-shadow: none !important;
        }

    </style>
@endsection

@section('content')
    <div class="container">

        <div class="card" style="height: 100% !important; border: none !important; box-shadow: none !important;">
            <div class="header">
                {{-- <h2 class="pull-left"> --}}
                <h2 style="display: inline-block !important">
                    Programme: <strong class="col-green">{{ 'Programme 01' }}</strong> <a
                        href="{{ route('pf.programs.data.create', ['program' => 'f8f69c55-e329-4043-a534-d319da3dbfb5']) }}"
                        class="btn bg-green btn-xs waves-effect p-l-5"><i class="material-icons"
                            style="font-size: 15px !important">add</i>Nouvelle donnée</a>
                    <small> Liste des structures intervenant :
                        @foreach (range(1, rand(2, 4)) as $structure)
                            <span class="badge">{{ 'Structure ' . $structure }}</span>
                        @endforeach
                    </small>
                </h2>

                <a href="{{ route('pf.programs.data.index', ['program' => 'f8f69c55-e329-4043-a534-d319da3dbfb5']) }}"
                    class="btn bg-red btn-xs waves-effect pull-right">
                    <i class="material-icons">keyboard_backspace</i>
                    <span>Retour</span>
                </a>
            </div>

            <div class="body">

                <div class="table-responsive">
                    <table class="table table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Désignation</th>
                                <th>Unité</th>
                                <th>Structure Responsable</th>
                                <th>Indicateur</th>
                                <th>source</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Désignation</th>
                                <th>Unité</th>
                                <th>Structure Responsable</th>
                                <th>source</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @php
                                $counta = rand(25, 40);
                                $itab = range(1, $counta);
                                rsort($itab);
                            @endphp

                            @foreach ($itab as $section)

                                @php
                                    $i = $counta - $loop->index + 1;
                                @endphp

                                <tr>
                                    <td>{{ $counta - $loop->index + 1 }}</td>

                                    <td> {{ 'Donnée ' . $i }}</td>

                                    <td data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Test tooltip">
                                        {{ rand(4, 10) }}</td>

                                    <td>{{ ['m', 'm2', 'kl', 'l', 'Kg', 'cm', 'T'][rand(0, 6)] }}</td>

                                    <td>{{ 'Structure ' . rand(1, 5) }}</td>

                                    <td>
                                        <span class="badge {{ rand(0, 1) ? 'bg-green' : 'bg-blue' }}"
                                            data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="Contenu Source">
                                            {{ rand(0, 1) ? 'FOURNIS' : 'NON-FOURNIS' }}</span>
                                    </td>

                                    <td class="text-nowrap text-center">
                                        <a class="btn btn-xs btn-success"
                                            href="{{ route('pf.programs.data.index', ['program' => '2664266b-ea19-4ac7-a8bc-4a393881b16d']) }}"><i
                                                class="material-icons">edit</i></a>
                                        <a class="btn btn-xs bg-indigo" href="#"><i class="material-icons">send</i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('js')

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Jequry Taost Js -->
    <script src="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset('plugins/momentjs/moment-with-locales.min.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}">
    </script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>


    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <script src="{{ asset('js/pages/forms/form-wizard.js') }}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable-c.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>

    <script>
        @if (session('success'))
            $.toast({
            heading: 'Succès',
            text: "{{ session('success') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            bgColor: "#4CAF50",
            icon: 'success'
            })
        @endif

        @if (session('error'))
            $.toast({
            heading: 'Erreur',
            text: "{{ session('error') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'error'
            })
        @endif

        @if (session('warning'))
            $.toast({
            heading: 'Attention',
            text: "{{ session('warning') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'warning'
            })
        @endif


        $(function() {
            //Tooltip
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
        })
    </script>

@endsection
