@extends('base.base-without-header')

@section('title', 'Formulaire de valeurs')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- Waves Effect Css -->
    <link rel="stylesheet" href="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.css') }}">

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

    </style>

@endsection

@section('content')
    <div class="container">
        <div class="card" style="height: 100% !important; border: none !important; box-shadow: none !important;">
            <div class="header">
                {{-- <h2 class="pull-left"> --}}
                <h2 style="display: inline-block !important">
                    Donnée: <strong class="col-green">{{ $data->name }} - <span
                            class="badge bg-green">{{ $data->program->name }}</span></strong>
                    <small> Liste des structures intervenant :
                        @foreach ($data->program->structures as $structure)
                            <span class="badge">{{ $structure->name }}</span>
                        @endforeach
                    </small>
                </h2>

                <a href="{{ route('pf.programs.data.index', ['program' => $data->program->id]) }}"
                    class="btn bg-red btn-xs waves-effect pull-right">
                    <i class="material-icons">keyboard_backspace</i>
                    <span>Retour</span>
                </a>
            </div>

            <div class="body container">
                <div class="block-header">
                    <h2 class="pull-left">
                        <span class="btn bg-green btn-xs"><u>FORMULAIRE DE VALEURS</u></span>
                    </h2>
                    <ol class="breadcrumb pull-right">
                        <li><a href="{{ route('pf.collectors.index') }}">Accueil</a></li>
                        <li><a
                                href="{{ route('pf.programs.data.index', ['program' => $data->program->id]) }}">Programme</a>
                        </li>
                        <li class="active">Formulaire de valeurs</li>
                    </ol>
                </div>
                <br>
                <div></div>

                <div>
                    <form class="form-horizontal"
                        action="{{ route('pf.programs.data.records.store', ['program' => $data->program->id, 'data' => $data->id]) }}"
                        method="POST">
                        @csrf

                        <div class="table-responsive">
                            <table class="table table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Période</th>
                                        <th>Valeur</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Période</th>
                                        <th>Valeur</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @php
                                        $records = isset($data) && isset($data->records) ? $data->records->sortBy('trimestre_number')->values() : collect();
                                    @endphp
                                    @foreach (range(1, 4) as $row)

                                        <tr>
                                            <td>{{ $row }}</td>

                                            <td> {{ 'Trimestre n* ' . $row }} </td>

                                            <td><input type="text" name="data_record_{{ $row }}" id=""
                                                    class="form-control"
                                                    value="{{ $records->get($row - 1) ? $records->get($row - 1)->value : 0 }}">
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row clearfix">
                            <div class="pull-right m-r-10">
                                <button type="submit" class="btn btn-primary waves-effect">Enregistrer</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Jequry Taost Js -->
    <script src="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset('plugins/momentjs/moment-with-locales.min.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}">
    </script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>


    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <script src="{{ asset('js/pages/forms/form-wizard.js') }}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable-c.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>

    <script>
        @if (session('success'))
            $.toast({
            heading: 'Succès',
            text: "{{ session('success') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            bgColor: "#4CAF50",
            icon: 'success'
            })
        @endif

        @if (session('error'))
            $.toast({
            heading: 'Erreur',
            text: "{{ session('error') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'error'
            })
        @endif

        @if (session('warning'))
            $.toast({
            heading: 'Attention',
            text: "{{ session('warning') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'warning'
            })
        @endif


        $(function() {
            //Tooltip
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
        })
    </script>

@endsection
