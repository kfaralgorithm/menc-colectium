@extends('base.base-without-header')

@section('title', $is_creation ? 'Creation d\'une donnée' : 'Mise à jour d\'une donnée')

@section('css')

    <!-- CSS-->
    <!-- Google Fonts -->
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icon.css') }}">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('plugins/node-waves/waves.css') }}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{ asset('plugins/animate-css/animate.css') }}" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="{{ asset('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />

    <link rel="stylesheet" href="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.css') }}">
    <!-- Custom Css -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link rel="stylesheet" href="{{ asset('css/themes/all-themes.css') }}">
    <style>
        .odd .dataTables_empty {
            text-align: center;
        }

        a[href="#next"] {
            background-color: #4CAF50 !important;
        }

        .wizard .steps .current a {
            background-color: #4CAF50 !important;
        }

        .pagination {
            display: flex !important;
        }

        .wizard .content {
            border: none !important;
        }

        .dataTables_paginate.paging_simple_numbers {
            display: flex !important;
            flex-direction: column !important;
            align-items: flex-end !important;
        }

        .table-responsive {
            overflow-x: unset !important;
        }

        .pagination li.active a {
            background-color: #3F51B5 !important;
            border-color: #3F51B5 !important;
        }


        .pagination li.active a:hover {
            background-color: #1F2B82 !important;
            border-color: #1F2B82 !important
        }


        .dataTables_empty {
            text-align: center !important;
        }

    </style>

@endsection

@section('content')
    <div class="container">
        <div class="card" style="height: 100% !important; border: none !important; box-shadow: none !important;">
            <div class="header">
                {{-- <h2 class="pull-left"> --}}
                <h2 style="display: inline-block !important">
                    Programme: <strong class="col-green">{{ $program->name }}</strong>
                    <small> Liste des structures intervenant :
                        @foreach ($program->structures as $structure)
                            <span class="badge">{{ $structure->name }}</span>
                        @endforeach
                    </small>
                </h2>

                <a href="{{ route('pf.programs.data.index', ['program' => $program->id]) }}"
                    class="btn bg-red btn-xs waves-effect pull-right">
                    <i class="material-icons">keyboard_backspace</i>
                    <span>Retour</span>
                </a>
            </div>

            <div class="body container">
                <div class="block-header">
                    <h2 class="pull-left">
                        <span class="btn bg-green btn-xs"><u>
                                {{ $is_creation ? 'CREATION DE DONNEE' : 'MISE A JOUR DE DONNEE' }}</u></span>
                    </h2>
                    <ol class="breadcrumb pull-right">
                        <li><a href="{{ route('pf.collectors.index') }}">Accueil</a></li>
                        <li><a href="{{ route('pf.programs.data.index', ['program' => $program->id]) }}">Programme</a>
                        </li>
                        <li class="active">
                            {{ $is_creation ? 'Creation d\'une donnée' : 'Mise à jour d\'une donnée' }}
                        </li>
                    </ol>
                </div>
                <br>
                <div></div>

                <div>
                    <form class="form-horizontal" method="POST"
                        action="{{ $is_creation ? route('pf.programs.data.store', ['program' => $program->id]) : route('pf.programs.data.update', ['program' => $program->id, 'data' => $data->id]) }}">
                        @csrf
                        @if (!$is_creation)
                            @method("PUT")
                        @endif

                        <div class="row">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-data-name">Nom <span
                                                class="col-red">*</span></label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="create-or-update-data-name"
                                                    name="create_or_update_data_name" class="form-control"
                                                    placeholder="Entrez le nom de la donnée"
                                                    value="{{ isset($data) ? $data->name : '' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-data-collect-method">Méthode de collecte </label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth"
                                                    placeholder="Entrez la méthode de collecte de la donnée"
                                                    id="create-or-update-data-collect-method"
                                                    name="create_or_update_data_collect_method">{{ isset($data) ? $data->collect_method : '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-data-description">Description </label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth"
                                                    placeholder="Entrez la description de la donnée"
                                                    id="create-or-update-data-description"
                                                    name="create_or_update_data_description">{{ isset($data) ? $data->description : '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-data-source">Source </label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="1" class="form-control no-resize auto-growth"
                                                    placeholder="Entrez la source de la donnée"
                                                    id="create-or-update-data-source"
                                                    name="create_or_update_data_source">{{ isset($data) ? $data->source : '' }}</textarea>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                        <label for="create-or-update-data-unit">Unité <span
                                                class="col-red">*</span></label>
                                    </div>
                                    <div class="col-lg-9 col-md-10 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <select class="form-control show-tick ms" data-live-search="true"
                                                title="Sélectionnez l'unité de la donnée" id="create-or-update-data-unit"
                                                name="create_or_update_data_unit">
                                                <option value="" {{ $is_creation ? 'selected' : '' }}>Sélectionnez
                                                    l'unité de
                                                    la donnée
                                                </option>

                                                @foreach ($units->sortByDesc('name') as $unit)
                                                    <option value="{{ $unit->id }}">
                                                        {{ $unit->name }}{{ $unit->symbol ? ' - ' . $unit->symbol : '' }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if ($indicators->isNotEmpty())
                                <div class="row">
                                    <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                                <label for="create-or-update-data-indicator">Indicateur associé </label>
                                            </div>
                                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                                <div class="form-group">
                                                    <select class="form-control show-tick ms" data-live-search="true"
                                                        title="Sélectionnez l'indicateur associé à la donnée"
                                                        id="create-or-update-data-indicator"
                                                        name="create_or_update_data_indicator">
                                                        <option value="" {{ $is_creation ? 'selected' : '' }}>
                                                            Sélectionnez
                                                            l'indicateur associé à la donnée
                                                        </option>
                                                        @foreach ($indicators->sortByDesc('name') as $indicator)
                                                            <option value="{{ $indicator->id }}">
                                                                {{ $indicator->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 m-l-10">
                                <input type="checkbox" id="create-or-update-data-enable" name="create_or_update_data_enable"
                                    class="filled-in chk-col-green"
                                    {{ $is_creation || (isset($data) && $data->is_enable == 1) ? 'checked' : '' }}>
                                <label for="create-or-update-data-enable">Activé</label>
                            </div>
                        </div>

                        <div class="row clearfix">
                            <div class="pull-right m-r-20">
                                <button type="submit"
                                    class="btn btn-primary m-t-15 waves-effect">{{ $is_creation ? 'Enregistrer' : 'Mettre à jour' }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <!-- Jquery Core Js -->
    <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Jequry Taost Js -->
    <script src="{{ asset('plugins/jquery-toast/dist/jquery.toast.min.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

    <!-- Multi Select Plugin Js -->
    <script src="{{ asset('plugins/multi-select/js/jquery.multi-select.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('plugins/node-waves/waves.js') }}"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset('plugins/momentjs/moment-with-locales.min.js') }}"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}">
    </script>

    <!-- JQuery Steps Plugin Js -->
    <script src="{{ asset('plugins/jquery-steps/jquery.steps.js') }}"></script>

    <!-- Custom Js -->
    <script src="{{ asset('js/admin.js') }}"></script>


    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('plugins/jquery-validation/jquery.validate.js') }}"></script>

    <script src="{{ asset('js/pages/forms/form-wizard.js') }}"></script>
    <script src="{{ asset('js/pages/tables/jquery-datatable-c.js') }}"></script>

    <!-- Demo Js -->
    <script src="{{ asset('js/demo.js') }}"></script>

    <script>
        @if (session('success'))
            $.toast({
            heading: 'Succès',
            text: "{{ session('success') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            bgColor: "#4CAF50",
            icon: 'success'
            })
        @endif

        @if (session('error'))
            $.toast({
            heading: 'Erreur',
            text: "{{ session('error') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'error'
            })
        @endif

        @if (session('warning'))
            $.toast({
            heading: 'Attention',
            text: "{{ session('warning') }}",
            hideAfter: 10000,
            position: 'bottom-right',
            showHideTransition: 'slide',
            icon: 'warning'
            })
        @endif


        $(function() {
            //Tooltip
            $('[data-toggle="tooltip"]').tooltip({
                container: 'body'
            });
        })
    </script>

@endsection
