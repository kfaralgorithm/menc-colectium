<!-- Default Size -->
<div class="modal fade" id="modal-list-or-create" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-list-or-create-title">Liste ou création</h4>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect">ENREGISTRER</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">FERMER</button>
            </div>
        </div>
    </div>
</div>
