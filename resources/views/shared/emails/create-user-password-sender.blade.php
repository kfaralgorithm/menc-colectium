@component('mail::message')
<div style="text-align: center">
    <img style="text-align: center;width: 100px;height: 100px;display: block;margin: auto" src="{{ asset('customer/img/password-mail.png') }}" style="height:150px;width: 150px;" alt="email" /> <br>
</div>
<p style="text-align: center;color: black;">
    Votre mot de passe: <br>
    <span style="font-weight: bold">{{ $password }} </span>
</p>

<p style="margin-top: 5px; color: black;text-align: center; margin-bottom: 20px">
    Avez-vous besoin d'aide ? Envoyez nous un message à <span style="font-weight: bold">{{ $contact }}</span> <br>
</p>
<div style="text-align: center;">
    <small>Vous recevez cet email parce que vous avez été enregistré dans le systeme <strong>SigUDP</strong>. <span style="font-weight: bold;color: black"> Si vous ne reconnaissez pas le système SigUDP, merci de ignorer ce message.</span></small>
</div>

@endcomponent
