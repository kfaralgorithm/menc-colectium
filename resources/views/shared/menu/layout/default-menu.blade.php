<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">NAVIGATION</li>
        <li class="{{ $activa == 'home' || $activa == '' ? 'active' : '' }}">
            <a href="{{ route('admin.index') }}">
                <i class="material-icons">home</i>
                <span>Accueil</span>
            </a>
        </li>

        <li class="{{ $activa == 'structures' || $activa == 'structure-types' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">widgets</i>
                <span>Structures et types</span>
            </a>
            <ul class="ml-menu">
                <li class=" {{ $activa == 'structures' ? 'active' : '' }}">
                    <a href="{{ route('admin.structures.index') }}">
                        @if ($activa != 'structures')
                            <i class="material-icons">business</i>
                        @endif
                        <span>Structures</span>
                    </a>
                </li>
                <li class=" {{ $activa == 'structure-types' ? 'active' : '' }}">
                    <a href="{{ route('admin.structure-types.index') }}">
                        @if ($activa != 'structure-types')
                            <i class="material-icons">title</i>
                        @endif
                        <span>Types de structure</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="{{ $activa == 'programs' ? 'active' : '' }}">
            <a href="{{ route('admin.programs.index') }}">
                <i class="material-icons">chrome_reader_mode</i>
                <span>Programmes</span>
            </a>
        </li>

        <li class="{{ $activa == 'units' || $activa == 'indicators' || $activa == 'data' ? 'active' : '' }}">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">graphic_eq</i>
                <span>Statistique</span>
            </a>
            <ul class="ml-menu">
                <li class="{{ $activa == 'units' ? 'active' : '' }}">
                    <a href="{{ route('admin.units.index') }}">
                        @if ($activa != 'units')
                            <i class="material-icons">format_underlined</i>
                        @endif
                        <span>Unités</span>
                    </a>
                </li>


                <li class=" {{ $activa == 'indicators' ? 'active' : '' }}">
                    <a href="{{ route('admin.indicators.index') }}">
                        @if ($activa != 'indicators')
                            <i class="material-icons">data_usage</i>
                        @endif
                        <span>Indicateurs</span>
                    </a>
                </li>

                <li class=" {{ $activa == 'data' ? 'active' : '' }}">
                    <a href="#">
                        @if ($activa != 'data')
                            <i class="material-icons">grain</i>
                        @endif
                        <span>Données</span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="pages/changelogs.html">
                <i class="material-icons">help</i>
                <span>Documentation</span>
            </a>
        </li>
    </ul>
</div>
<!-- #Menu -->
