<!-- User Info -->
<div class="user-info">
    <div class="image">
        <img src="{{ asset('images/only-logo-wf.png') }}" width="48" height="48" alt="User" />
    </div>
    <div class="info-container">
        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ auth()->user()->structure->name }}</div>
        <div class="email"><span class="badge bg-blue">{{ auth()->user()->username }}</span></div>
        <div class="user-helper-dropdown">
            <a href="{{ route('auth.logout') }}"
                class="btn bg-red btn-circle waves-effect waves-green waves-circle waves-float pull-right"
                style="margin-bottom: 10px;">
                <i class="material-icons">input</i>
            </a>
        </div>
    </div>
</div>
<!-- #User Info -->
