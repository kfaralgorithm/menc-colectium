    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            @include('shared.menu.layout.default-user-info')
            <!-- #User Info -->

            <!-- Menu -->
            @include('shared.menu.layout.default-menu')
            <!-- #Menu -->

            <!-- Footer -->
            @include('shared.menu.layout.default-footer')
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->

        <!-- Right Sidebar -->
        @include('shared.menu.setting')
        <!-- #END# Right Sidebar -->
    </section>
