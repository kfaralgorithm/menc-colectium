    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">SIG UDP </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-left">
                    <li style="padding-right: 155px">
                        <!-- Sidebar toggle button-->
                        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"><span
                                class="sr-only"></span></a>
                    </li>
                    <li class="nav-item" style="border-bottom: 5px solid #666 !important;">
                        <a class="nav-link text-light font-weigth-bold px-3" href="">ACCUEIL</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  text-light font-weigth-bold" href="">COLLECTE</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-light font-weigth-bold" href="">INDICATEURS</a>
                    </li>
                    <li class="nav-item active"><a class="nav-link  text-light font-weigth-bold" href="">PARAMETRES</a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li>
                        <a href="" class="btn bg-red btn-xs waves-effect pull-right">
                            <i class="material-icons">logout</i>
                            <span>Déconnecter</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
