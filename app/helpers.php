<?php

function datetime_format_system()
{
    return 'd/m/Y H:i:s';
}

function date_format_system()
{
    return 'd/m/Y';
}

function datetime_format_db()
{
    return 'Y-m-d H:i:s';
}

function date_format_db()
{
    return 'Y-m-d';
}


if (!function_exists('post_client')) {
    function post_client(string $url, $params)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->post($url, $params);

        return json_decode((string)$response->getBody());
    }
}


function datetime_db_to_system_format($date)
{
    return \Carbon\Carbon::createFromFormat(datetime_format_db(), $date)->format(datetime_format_system());
}


function datetime_db_to_system_date_format($date)
{
    return \Carbon\Carbon::createFromFormat(datetime_format_db(), $date)->format(date_format_system());
}

if (!function_exists('generate_strong_password')) {
    function generate_strong_password($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if (strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if (strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if (strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }

        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];

        $password = str_shuffle($password);

        if (!$add_dashes)
            return $password;

        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
}
