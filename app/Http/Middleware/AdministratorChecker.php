<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class AdministratorChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            $role_id = DB::table('role_user')->where('user_id', auth()->user()->id)->where('is_current', true)->first();

            if ($role_id == null) {
                auth()->logout();
                return redirect()->route('auth.login')->with('error', 'Veuillez contacter l\'administrateur pour la finalisation de la configuration de votre compter.');
            }

            $role = Role::find($role_id->role_id);

            if ($role->name == 'ADMINISTRATEUR') {
                return $next($request);
            }
            auth()->logout();
        }
        return redirect()->route('auth.login')->with('error', 'Veuillez vous connecter ou vous enregistrer avant d\'avoir accès à cette page.');
    }
}
