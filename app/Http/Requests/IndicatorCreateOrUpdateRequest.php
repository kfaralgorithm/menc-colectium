<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndicatorCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->route('indicator') && \Ramsey\Uuid\Uuid::isValid(request()->route('indicator'))) {
            return [
                'create_or_update_indicator_name' => 'nullable|min:3|max:250|unique:indicators,name,' . request()->route('indicator'),
                'create_or_update_indicator_description' => 'nullable|max:250',
                'create_or_update_indicator_enable' => 'in:on',
            ];
        }


        return [
            'create_or_update_indicator_name' => 'required|max:250|unique:indicators,name',
            'create_or_update_indicator_description' => 'nullable|min:3|max:250',
            'create_or_update_indicator_enable' => 'nullable|in:on',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
