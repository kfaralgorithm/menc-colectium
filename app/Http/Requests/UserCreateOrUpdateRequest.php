<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->route('user') && \Ramsey\Uuid\Uuid::isValid(request()->route('user'))) {
            return [
                'create_or_update_user_firstname' => 'nullable|min:2|max:250',
                'create_or_update_user_lastname' => 'nullable|min:2|max:250',
                'create_or_update_user_phone' => 'nullable|min:2|max:250',
                'create_or_update_user_enable' => 'nullable|in:on',
            ];
        }

        return [
            'create_or_update_user_firstname' => 'required|min:2|max:250',
            'create_or_update_user_lastname' => 'required|min:2|max:250',
            'create_or_update_user_email' => 'required|email|min:2|max:250|unique:users,email',
            'create_or_update_user_phone' => 'required|min:2|max:250',
            'create_or_update_user_enable' => 'nullable|in:on',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
