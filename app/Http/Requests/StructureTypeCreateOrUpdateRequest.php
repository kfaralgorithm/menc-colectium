<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StructureTypeCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->route('structure_type') && \Ramsey\Uuid\Uuid::isValid(request()->route('structure_type'))) {
            return [
                'create_or_update_type_structure_name' => 'nullable|min:3|max:250|unique:structure_types,name,' . request()->route('structure_type'),
                'create_or_update_type_structure_description' => 'nullable|max:250',
                'create_or_update_type_structure_enable' => 'in:on',
            ];
        }


        return [
            'create_or_update_type_structure_name' => 'required|max:250|unique:structure_types,name',
            'create_or_update_type_structure_description' => 'nullable|min:3|max:250',
            'create_or_update_type_structure_enable' => 'nullable|in:on',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
