<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MembersAddOrRemoveRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (isset(request()->_method) && strtoupper(request()->_method) == "DELETE") {
            return [
                'members_add_or_remove_program' => 'required|uuid|exists:structures,id',
            ];
        }
        return [
            'members_add_or_remove_program' => 'required|array',
        ];
    }



    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
