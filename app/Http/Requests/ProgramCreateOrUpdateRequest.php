<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProgramCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->route('program') && \Ramsey\Uuid\Uuid::isValid(request()->route('program'))) {
            return [
                'create_or_update_program_name' => 'nullable|min:3|max:250|unique:programs,name,' . request()->route('program'),
                'create_or_update_program_description' => 'nullable|max:250',
                'create_or_update_program_enable' => 'in:on',
                'create_or_update_program_objective' => 'nullable|min:1|max:250',
            ];
        }

        //dd(10, request()->all());


        return [
            'create_or_update_program_name' => 'required|max:250|unique:programs,name',
            'create_or_update_program_description' => 'nullable|min:3|max:250',
            'create_or_update_program_enable' => 'nullable|in:on',
            'create_or_update_program_objective' => 'required|min:1|max:250',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
