<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DataCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->route('data') && \Ramsey\Uuid\Uuid::isValid(request()->route('data'))) {
            return [
                'create_or_update_data_name' => 'nullable|max:250|unique:data,name,' . request()->route('data'),
                'create_or_update_data_description' => 'nullable|min:3|max:250',
                'create_or_update_data_collect_method' => 'nullable|min:3|max:250',
                'create_or_update_data_source' => 'nullable|min:3|max:250',
                'create_or_update_data_enable' => 'nullable|in:on',
                'create_or_update_data_unit' => 'nullable|uuid|exists:units,id',
                'create_or_update_data_indicator' => 'nullable|uuid|exists:indicators,id',
            ];
        }

        return [
            'create_or_update_data_name' => 'required|max:250|unique:data,name',
            'create_or_update_data_description' => 'nullable|min:3|max:250',
            'create_or_update_data_collect_method' => 'nullable|min:3|max:250',
            'create_or_update_data_source' => 'nullable|min:3|max:250',
            'create_or_update_data_enable' => 'nullable|in:on',
            'create_or_update_data_unit' => 'required|uuid|exists:units,id',
            'create_or_update_data_indicator' => 'nullable|uuid|exists:indicators,id',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
