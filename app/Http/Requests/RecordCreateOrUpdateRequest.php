<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecordCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_record_4' => 'required_without_all:data_record_1,data_record_2, data_record_3',
            'data_record_3' => 'required_without_all:data_record_4,data_record_1,data_record_2',
            'data_record_2' => 'required_without_all:data_record_3,data_record_4,data_record_1',
            'data_record_1' => 'required_without_all:data_record_2,data_record_3,data_record_4',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
