<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StructureCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->route('structure') && \Ramsey\Uuid\Uuid::isValid(request()->route('structure'))) {
            return [
                'create_or_update_structure_name' => 'nullable|string|min:2|max:250|unique:structures,name,' . request()->route('structure'),
                'create_or_update_structure_password' => 'nullable|min:2|max:50',
                'create_or_update_structure_type' => 'sometimes|uuid|exists:structure_types,id',
                'create_or_update_structure_enable' => 'nullable|in:on',
            ];
        }

        return [
            'create_or_update_structure_name' => 'required|string|min:2|max:250|unique:structures,name',
            //'create_or_update_structure_parent' => 'nullable|exists:structures,id',
            'create_or_update_structure_password' => 'required|min:2|max:50',
            'create_or_update_structure_type' => 'required|uuid|exists:structure_types,id',
            'create_or_update_structure_enable' => 'nullable|in:on',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
