<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnitCreateOrUpdateRequest extends FormRequest
{
    public $validator = null;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if (request()->route('unit') && \Ramsey\Uuid\Uuid::isValid(request()->route('unit'))) {
            return [
                'create_or_update_unit_name' => 'nullable|min:3|max:250|unique:units,name,' . request()->route('unit'),
                'create_or_update_unit_description' => 'nullable|max:250',
                'create_or_update_unit_enable' => 'in:on',
                'create_or_update_unit_symbol' => 'nullable|min:1|max:250|unique:units,symbol,' . request()->route('unit'),
            ];
        }

        return [
            'create_or_update_unit_name' => 'required|max:250|unique:units,name',
            'create_or_update_unit_description' => 'nullable|min:3|max:250',
            'create_or_update_unit_enable' => 'nullable|in:on',
            'create_or_update_unit_symbol' => 'required|min:1|max:250|unique:units,symbol',
        ];
    }

    public function failedValidation(\Illuminate\Contracts\Validation\Validator $validator)
    {
        $this->validator = $validator;
    }
}
