<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function default()
    {
        if (auth()->check()) {
            $role_id = DB::table('role_user')->where('user_id', auth()->user()->id)->where('is_current', true)->first();

            if ($role_id == null) {
                auth()->logout();
                return redirect()->route('auth.login')->with('error', 'Veuillez contacter l\'administrateur pour la finalisation de la configuration de votre compter.');
            }

            $role = Role::find($role_id->role_id);

            if ($role->name == 'COLLECTEUR') {
                return redirect()->route('pf.collectors.index')->with('success', 'Bienvenu ' . auth()->user()->username);
            } else if ($role->name == 'ADMINISTRATEUR') {
                return redirect()->route('admin.index')->with('success', 'Bienvenu ' . auth()->user()->username);
            }

            auth()->logout();
        }
        return redirect()->route('auth.login')->with('error', 'Veuillez vous connecter ou vous enregistrer avant d\'avoir accès à cette page.');
    }
}
