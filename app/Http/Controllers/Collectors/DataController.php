<?php

namespace App\Http\Controllers\Collectors;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\DataRepositoryInterface;
use App\Interfaces\UnitRepositoryInterface;
use App\Interfaces\ProgramRepositoryInterface;
use App\Http\Requests\DataCreateOrUpdateRequest;
use App\Interfaces\IndicatorRepositoryInterface;
use App\Interfaces\StructureRepositoryInterface;

class DataController extends Controller
{
    protected $programRepository;
    protected $dataRepository;
    protected $unitRepository;
    protected $structureRepository;
    protected $indicatorRepository;

    /**
     * Class constructor.
     */
    public function __construct(ProgramRepositoryInterface $programRepository, DataRepositoryInterface $dataRepository, UnitRepositoryInterface $unitRepository, StructureRepositoryInterface $structureRepository, IndicatorRepositoryInterface $indicatorRepository)
    {
        $this->programRepository = $programRepository;
        $this->dataRepository = $dataRepository;
        $this->unitRepository = $unitRepository;
        $this->structureRepository = $structureRepository;
        $this->indicatorRepository = $indicatorRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index(Request  $request, $id)
    {
        $program = $this->programRepository->getById($id)['data'];
        return view('modules.collectors.data.index')->with(compact(['program']));
    }


    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index_with_values(Request  $request, $id)
    {
        $program = $this->programRepository->getById($id)['data'];
        return view('modules.collectors.data.index-values')->with(compact(['program']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  uuid  $program
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $program)
    {
        $is_creation = true;
        $program = $this->programRepository->getById($program)['data'];
        $units = $this->unitRepository->getList($request)['data'];
        $structures = $this->structureRepository->getList($request)['data'];
        $indicators = $this->indicatorRepository->getList($request)['data'];

        return view('modules.collectors.data.create-or-update')->with(compact(['is_creation', 'program', 'units', 'structures', 'indicators']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DataCreateOrUpdateRequest  $request
     * @param  uuid  $program
     * @return \Illuminate\Http\Response
     */
    public function store(DataCreateOrUpdateRequest $request, $program)
    {
        $is_creation = true;
        $rlts = $this->dataRepository->createOrUpdate($request, $program);

        return redirect()->route('pf.programs.data.create', ['program' => $program])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $program
     * @param  int  $data
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $program, $data)
    {
        $is_creation = false;

        $rlts = $this->programRepository->getById($program);

        if ($rlts['error']) {
            return redirect()->route('admin.programs.index')->with('error', $rlts['message']);
        }
        $program = $rlts['data'];



        $rlts = $this->dataRepository->getById($data);

        if ($rlts['error']) {
            return redirect()->route('admin.programs.index')->with('error', $rlts['message']);
        }

        $data = $rlts['data'];
        $units = $this->unitRepository->getList($request)['data'];
        $structures = $this->structureRepository->getList($request)['data'];
        $indicators = $this->indicatorRepository->getList($request)['data'];

        // dd($program, isset($program));
        return view('modules.collectors.data.create-or-update')->with(compact(['is_creation', 'program', 'units', 'structures', 'indicators', 'data']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DataCreateOrUpdateRequest  $request
     * @param  uuid  $program
     * @param  uuid  $data
     * @return \Illuminate\Http\Response
     */
    public function update(DataCreateOrUpdateRequest $request, $program, $data)
    {
        $rlts = $this->dataRepository->createOrUpdate($request, $program, $data);
        return redirect()->route('pf.programs.data.edit', ['program' => $program, 'data' => $data])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
