<?php

namespace App\Http\Controllers\Collectors;

use App\Models\Data;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RecordCreateOrUpdateRequest;
use App\Interfaces\DataRepositoryInterface;
use App\Interfaces\UnitRepositoryInterface;
use App\Interfaces\RecordRepositoryInterface;
use App\Interfaces\ProgramRepositoryInterface;
use App\Interfaces\IndicatorRepositoryInterface;
use App\Interfaces\StructureRepositoryInterface;

class RecordController extends Controller
{
    protected $programRepository;
    protected $dataRepository;
    protected $unitRepository;
    protected $structureRepository;
    protected $indicatorRepository;
    protected $recordRepository;

    /**
     * Class constructor.
     */
    public function __construct(
        ProgramRepositoryInterface $programRepository,
        DataRepositoryInterface $dataRepository,
        UnitRepositoryInterface $unitRepository,
        StructureRepositoryInterface $structureRepository,
        IndicatorRepositoryInterface $indicatorRepository,
        RecordRepositoryInterface $recordRepository
    ) {
        $this->programRepository = $programRepository;
        $this->dataRepository = $dataRepository;
        $this->unitRepository = $unitRepository;
        $this->structureRepository = $structureRepository;
        $this->indicatorRepository = $indicatorRepository;
        $this->recordRepository = $recordRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  uuid  $program
     * @param  uuid  $data
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $program, $data)
    {
        $is_creation = true;
        $data = $this->dataRepository->getById($data)['data'];
        //dd($data->records->sortByDesc('trimestre_number')->toArray(), $data->records->sortBy('trimestre_number')->values()->toArray());
        return view('modules.collectors.data.records.create-or-update')->with(compact(["data"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RecordCreateOrUpdateRequest  $request
     * @param  uuid  $program
     * @param  uuid  $data
     * @return \Illuminate\Http\Response
     */
    public function store(RecordCreateOrUpdateRequest $request, $program, $data)
    {
        //dd($request->all());
        $rlts = $this->recordRepository->createOrUpdate($request, $data);

        return redirect()->route('pf.programs.data.records.create', ['program' => $program, "data" => $data])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
