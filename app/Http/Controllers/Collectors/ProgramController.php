<?php

namespace App\Http\Controllers\Collectors;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ProgramRepositoryInterface;

class ProgramController extends Controller
{


    protected $programRepository;

    /**
     * Class constructor.
     */
    public function __construct(ProgramRepositoryInterface $programRepository)
    {
        $this->programRepository = $programRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programs = $this->programRepository->getList($request)['data'];
        return view('modules.collectors.index')->with(compact('programs'));
    }
}
