<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Notifications\UserChangePasswordLinkNotif;

class ForgetPasswordController extends Controller
{
    public function forgot_password()
    {
        return view('modules.auth.forgot-password');
    }


    public function send_token(Request $request)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request->all(), [
                'forgot_password_email' => 'required|email',
            ]);

            if ($validator->fails()) {
                return redirect()->route('password.forgot')->with('error', $validator->errors()->first());
            }

            $user = User::where('email', '=', $request->forgot_password_email)->where('is_enable', true)->first();

            //Check if the user exists
            if (!$user) {
                return redirect()->route('password.forgot')->with('error', 'Cet email n\'est pas reconnu par le systeme.');
            }

            $random = Str::random(rand(50, 60));


            DB::table('password_resets')
                ->where('email', $request->forgot_password_email)
                ->update(['closed_at' => Carbon::now()]);

            //Create Password Reset Token
            DB::table('password_resets')->insert([
                'email' => $request->forgot_password_email,
                'token' => $random,
                'created_at' => Carbon::now()
            ]);


            $user->notify(new UserChangePasswordLinkNotif($random, $user->name));


            DB::commit();

            return redirect()->route('password.forgot')->with('success', 'Un email contenant un lien de réinitialisation de votre mot de passe a été envoyé dans votre boite mail. Veuillez consulter votre boite mail.');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', json_encode($e->getMessage()));
        }
    }


    public function reset_password($token)
    {
        $password_reset_record = DB::table('password_resets')
            ->where('token', $token)
            ->first();

        if (!$password_reset_record || $password_reset_record->used_at || $password_reset_record->closed_at || !$password_reset_record->created_at ||  (Carbon::now()->timestamp - (Carbon::createFromFormat(datetime_format_db(), $password_reset_record->created_at))->timestamp) > 3600) {
            return redirect()->route('password.forgot')->with('error', 'Ce lien n\'est plus valide.');
        }

        return view('modules.auth.user-reset-password')->with(compact(['token']));
    }

    public function save_new_password(Request $request, $token)
    {
        DB::beginTransaction();
        try {

            $validator = Validator::make($request->all(), [
                'new_password' => 'required|confirmed|min:6',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->with('error', $validator->errors()->first());
            }

            // Verification de la validité du token
            $password_reset_record = DB::table('password_resets')
                ->where('token', $token)
                ->first();

            if (!$password_reset_record || $password_reset_record->used_at || $password_reset_record->closed_at || !$password_reset_record->created_at || (Carbon::now()->timestamp - (Carbon::createFromFormat(datetime_format_db(), $password_reset_record->created_at))->timestamp) > 3600) {
                return redirect()->route('password.forgot')->with('error', 'Ce lien n\'est plus valide.');
            }

            // Verification de la validité de l'utilisateur
            $user = User::where('email', '=', $password_reset_record->email)->where('is_enable', true)->first();

            if (!$user) {
                return redirect()->route('password.forgot')->with('error', 'Cet email n\'est pas reconnu par le systeme.');
            }

            $user->update([
                'password' => bcrypt($request->new_password),
            ]);

            $affected = DB::table('password_resets')
                ->where('token', $password_reset_record->token)
                ->update(['used_at' => Carbon::now()]);

            DB::commit();

            return redirect()->route('login.view')->with('success', "Veuillez vous connecter avec votre nouveau mot de passe.");
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', json_encode($e->getMessage()));
        }
    }
}
