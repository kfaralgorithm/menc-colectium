<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    function create()
    {
        return view('modules.shared.auth.login');
    }


    function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'g-recaptcha-response' => 'required',
        ], [
            'username.required' => "Veuillez saisir le nom d'utilisateur.",
            'password.required' => "Veuillez saisir le mot de passe.",
            'g-recaptcha-response.required' => "Veuillez valider le Google reCAPTCHA.",
        ]);


        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first());
        }

        $grecaptcha = $request->{'g-recaptcha-response'};

        $body = $this->grecaptchOut($grecaptcha);

        if ($body->success == true) {
            $user = User::where(['username' => trim($request->username)])->first();
            if ($user) {

                $rolesl = DB::table('role_user')->where('user_id', $user->id)->where('is_current', true)->get();

                if ($rolesl->count() == 0) {
                    return redirect()->route('auth.login')->with('error', 'Probleme de configuration. Veuillez contacter l\'administrateur.');
                }

                $rolesl = $rolesl->first();

                $remember_me = $request->remember_me ? true : false;

                if (auth()->attempt($request->only('username', 'password'), $remember_me)) {
                    $role = Role::find($rolesl->role_id);

                    if ($role->name == 'COLLECTEUR') {
                        return redirect()->route('pf.collectors.index')->with('success', 'Bienvenu ' . $user->username);
                    } else if ($role->name == 'ADMINISTRATEUR') {
                        return redirect()->route('admin.index')->with('success', 'Bienvenu ' . $user->username);
                    }

                    auth()->logout();
                    \Session::flush();
                } else {
                    return redirect()->back()->with('error', 'Informations de connexion non valides.');
                }
            } else {
                return redirect()->back()->with('error', 'Informations de connexion non valides.');
            }
        } else {
            return redirect()->back()->with('error', 'Recaptcha non valide.');
        }
    }


    function grecaptchOut($grecaptcha)
    {
        $body = post_client('https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => env('GOOGLE_CAPTCHA_SECRET'),
                'response' => $grecaptcha
            ]
        ]);
        return $body;
    }
}
