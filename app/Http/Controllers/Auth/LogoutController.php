<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\SessionTracker;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    /**
     * logout function
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {

        // auth()->user()->sessionTrackers->where('ip_address', request()->getClientIp())->destroy();

        auth()->logout();
        \Session::flush();

        return redirect()->route('auth.login')->with('success', 'Déconnection réussie.');
    }
}
