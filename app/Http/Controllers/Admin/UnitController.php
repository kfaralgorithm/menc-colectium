<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UnitRepositoryInterface;
use App\Http\Requests\UnitCreateOrUpdateRequest;

class UnitController extends Controller
{
    protected $unitRepository;
    /**
     * Class constructor.
     */
    public function __construct(UnitRepositoryInterface $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $units = $this->unitRepository->getList($request)['data'];

        return view("modules.admin.units.index")->with(compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $is_creation = true;
        return view('modules.admin.units.create-or-update')->with(compact('is_creation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UnitCreateOrUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UnitCreateOrUpdateRequest $request)
    {
        $is_creation = true;
        $rlts = $this->unitRepository->createOrUpdate($request);


        //dd(89, request()->all(), $rlts, ($rlts['error'] ? 'error' : 'success'));

        return redirect()->route('admin.units.create')->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $is_creation = false;

        $rlts = $this->unitRepository->getById($id);

        if ($rlts['error']) {
            return redirect()->route('admin.units.index')->with('error', $rlts['message']);
        }

        $unit = $rlts['data'];

        // dd($unit, isset($unit));
        return view('modules.admin.units.create-or-update')->with(compact(['is_creation', 'unit']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UnitCreateOrUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnitCreateOrUpdateRequest $request, $id)
    {
        $is_creation = false;
        $rlts = $this->unitRepository->createOrUpdate($request, $id);
        $unit = $rlts['data'];
        return redirect()->route('admin.units.edit', ['unit' => $id])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact(['is_creation', 'unit']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
