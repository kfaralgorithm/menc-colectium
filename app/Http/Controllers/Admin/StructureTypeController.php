<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\StructureTypeRepositoryInterface;
use App\Http\Requests\StructureTypeCreateOrUpdateRequest;
use Session;

class StructureTypeController extends Controller
{
    protected $structureTypeRepository;
    /**
     * Class constructor.
     */
    public function __construct(StructureTypeRepositoryInterface $structureTypeRepository)
    {
        $this->structureTypeRepository = $structureTypeRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $types = $this->structureTypeRepository->getList($request)['data'];

        return view("modules.admin.structure-types.index")->with(compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $is_creation = true;
        return view('modules.admin.structure-types.create-or-update')->with(compact('is_creation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StructureTypeCreateOrUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StructureTypeCreateOrUpdateRequest $request)
    {
        $is_creation = true;
        $rlts = $this->structureTypeRepository->createOrUpdate($request);

        return redirect()->route('admin.structure-types.create')->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $is_creation = false;

        $rlts = $this->structureTypeRepository->getById($id);

        if ($rlts['error']) {
            return redirect()->route('admin.structure-types.index')->with('error', $rlts['message']);
        }

        $type = $rlts['data'];

        // dd($type, isset($type));
        return view('modules.admin.structure-types.create-or-update')->with(compact(['is_creation', 'type']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StructureTypeCreateOrUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StructureTypeCreateOrUpdateRequest $request, $id)
    {
        $is_creation = false;
        $rlts = $this->structureTypeRepository->createOrUpdate($request, $id);
        $type = $rlts['data'];
        return redirect()->route('admin.structure-types.edit', ['structure_type' => $id])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact(['is_creation', 'type']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
