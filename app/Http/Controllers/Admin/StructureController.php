<?php

namespace App\Http\Controllers\Admin;

use App\Models\Structure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ProgramsAddOrRemoveRequest;
use App\Interfaces\StructureRepositoryInterface;
use App\Interfaces\StructureTypeRepositoryInterface;
use App\Http\Requests\StructureCreateOrUpdateRequest;
use App\Interfaces\ProgramRepositoryInterface;

class StructureController extends Controller
{
    protected $structureTypeRepository;
    protected $structureRepository;
    protected $programRepository;
    /**
     * Class constructor.
     */
    public function __construct(StructureRepositoryInterface $structureRepository, StructureTypeRepositoryInterface $structureTypeRepository, ProgramRepositoryInterface $programRepository)
    {
        $this->structureRepository = $structureRepository;
        $this->structureTypeRepository = $structureTypeRepository;
        $this->programRepository = $programRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $structures = $this->structureRepository->getList($request)['data'];

        return view('modules.admin.structures.index')->with(compact(['structures']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request  $request)
    {
        $is_creation = true;
        $types = $this->structureTypeRepository->getList($request)['data'];
        $possible_parent  = $this->structureRepository->getList($request)['data'];
        return view('modules.admin.structures.create-or-update')->with(compact(['is_creation', 'types', 'possible_parent']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StructureCreateOrUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StructureCreateOrUpdateRequest $request)
    {
        $is_creation = true;
        $rlts = $this->structureRepository->createOrUpdate($request);


        //dd(89, request()->all(), $rlts, ($rlts['error'] ? 'error' : 'success'));

        return redirect()->route('admin.structures.create')->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request  $request, $id)
    {
        $is_creation = false;
        $rlts = $this->structureRepository->getById($id);
        if ($rlts['error']) {
            return redirect()->route('admin.structures.index')->with('error', $rlts['message']);
        }

        $structure = $rlts['data'];

        $types = $this->structureTypeRepository->getList($request)['data'];

        $possible_programs = $this->programRepository->getList($request)['data'];
        $possible_programs = $possible_programs->whereNotIn('id', $structure->programs->pluck('id')->toArray());

        // dd($structure, isset($structure));
        return view('modules.admin.structures.create-or-update')->with(compact(['is_creation', 'structure', 'types', 'possible_programs']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StructureCreateOrUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StructureCreateOrUpdateRequest $request, $id)
    {
        $is_creation = false;
        $rlts = $this->structureRepository->createOrUpdate($request, $id);
        $structure = $rlts['data'];
        return redirect()->route('admin.structures.edit', ['structure' => $id])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact(['is_creation', 'structure']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Add programs.
     *
     * @param  \App\Http\Requests\ProgramsAddOrRemoveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function programs_add(ProgramsAddOrRemoveRequest $request, $id)
    {
        $rlts = $this->structureRepository->programsAddOrRemove($request, $id);
        return Redirect::to(URL::previous() . "#associated-programs")->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }

    /**
     * Remove programs.
     *
     * @param  \App\Http\Requests\ProgramsAddOrRemoveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function programs_remove(ProgramsAddOrRemoveRequest $request, $id)
    {
        $rlts = $this->structureRepository->programsAddOrRemove($request, $id);
        return Redirect::to(URL::previous() . "#associated-programs")->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }
}
