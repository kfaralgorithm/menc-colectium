<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Structure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Builder;
use App\Interfaces\UserRepositoryInterface;
use App\Http\Requests\ProfilAddOrRemoveRequest;
use App\Http\Requests\UserCreateOrUpdateRequest;

class UserController extends Controller
{

    protected $userRepository;
    /**
     * Class constructor.
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(50);

        return view('modules.admin.users.list')->with(compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $is_creation = true;
        return view('modules.admin.users.create-or-update')->with(compact(['is_creation',]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserCreateOrUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateOrUpdateRequest $request)
    {
        $is_creation = true;
        $rlts = $this->userRepository->createOrUpdate($request);

        return redirect()->route('admin.users.create')->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $is_creation = false;

        $rlts = $this->userRepository->getById($id);

        if ($rlts['error']) {
            return redirect()->route('admin.users.index')->with('error', $rlts['message']);
        }

        $possible_profils = Structure::whereDoesntHave('profils')->orWhereHas('profils', function (Builder $query) use ($id) {
            $query->whereNotIn('owner_id', array($id));
        })->get();

        $user = $rlts['data'];

        //dd($user->structures->first(), $user->profils, isset($user));
        return view('modules.admin.users.create-or-update')->with(compact(['is_creation', 'user', 'possible_profils']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserCreateOrUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserCreateOrUpdateRequest $request, $id)
    {
        $is_creation = false;
        $rlts = $this->userRepository->createOrUpdate($request, $id);
        $user = $rlts['data'];
        return redirect()->route('admin.users.edit', ['user' => $id])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact(['is_creation', 'user']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Add profil.
     *
     * @param  \App\Http\Requests\ProfilAddOrRemoveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profils_add(ProfilAddOrRemoveRequest $request, $id)
    {
        $rlts = $this->userRepository->profilAddOrRemove($request, $id);
        return Redirect::to(URL::previous() . "#sub-profils")->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }

    /**
     * Remove profil.
     *
     * @param  \App\Http\Requests\ProfilAddOrRemoveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profils_remove(ProfilAddOrRemoveRequest $request, $id)
    {
        $rlts = $this->userRepository->profilAddOrRemove($request, $id);
        return Redirect::to(URL::previous() . "#sub-profils")->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }
}
