<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\IndicatorRepositoryInterface;
use App\Http\Requests\IndicatorCreateOrUpdateRequest;

class IndicatorController extends Controller
{
    protected $indicatorRepository;
    /**
     * Class constructor.
     */
    public function __construct(IndicatorRepositoryInterface $indicatorRepository)
    {
        $this->indicatorRepository = $indicatorRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $indicators = $this->indicatorRepository->getList($request)['data'];


        return view("modules.admin.indicators.index")->with(compact('indicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $is_creation = true;
        return view('modules.admin.indicators.create-or-update')->with(compact('is_creation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\IndicatorCreateOrUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IndicatorCreateOrUpdateRequest $request)
    {
        $is_creation = true;
        $rlts = $this->indicatorRepository->createOrUpdate($request);

        return redirect()->route('admin.indicators.create')->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $is_creation = false;

        $rlts = $this->indicatorRepository->getById($id);

        if ($rlts['error']) {
            return redirect()->route('admin.indicators.index')->with('error', $rlts['message']);
        }

        $indicator = $rlts['data'];

        // dd($indicator, isset($indicator));
        return view('modules.admin.indicators.create-or-update')->with(compact(['is_creation', 'indicator']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\IndicatorCreateOrUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IndicatorCreateOrUpdateRequest $request, $id)
    {
        $is_creation = false;
        $rlts = $this->indicatorRepository->createOrUpdate($request, $id);
        $indicator = $rlts['data'];
        return redirect()->route('admin.indicators.edit', ['indicator' => $id])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact(['is_creation', 'indicator']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
