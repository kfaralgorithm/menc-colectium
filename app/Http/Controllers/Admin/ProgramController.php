<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Interfaces\ProgramRepositoryInterface;
use App\Http\Requests\MembersAddOrRemoveRequest;
use App\Interfaces\StructureRepositoryInterface;
use App\Http\Requests\ProgramCreateOrUpdateRequest;

class ProgramController extends Controller
{
    protected $programRepository;
    protected $structureRepository;
    /**
     * Class constructor.
     */
    public function __construct(ProgramRepositoryInterface $programRepository, StructureRepositoryInterface $structureRepository)
    {
        $this->programRepository = $programRepository;
        $this->structureRepository = $structureRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programs = $this->programRepository->getList($request)['data'];

        return view("modules.admin.programs.index")->with(compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $is_creation = true;
        return view('modules.admin.programs.create-or-update')->with(compact('is_creation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ProgramCreateOrUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgramCreateOrUpdateRequest $request)
    {
        $is_creation = true;
        $rlts = $this->programRepository->createOrUpdate($request);


        //dd(89, request()->all(), $rlts, ($rlts['error'] ? 'error' : 'success'));

        return redirect()->route('admin.programs.create')->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact('is_creation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $is_creation = false;

        $rlts = $this->programRepository->getById($id);

        if ($rlts['error']) {
            return redirect()->route('admin.programs.index')->with('error', $rlts['message']);
        }
        $program = $rlts['data'];
        $possible_members = $this->structureRepository->getList($request)['data']->whereNotIn('id', $program->structures->pluck('id')->toArray());

        // dd($program, isset($program));
        return view('modules.admin.programs.create-or-update')->with(compact(['is_creation', 'program', 'possible_members']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\ProgramCreateOrUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProgramCreateOrUpdateRequest $request, $id)
    {
        $is_creation = false;
        $rlts = $this->programRepository->createOrUpdate($request, $id);
        $program = $rlts['data'];
        return redirect()->route('admin.programs.edit', ['program' => $id])->with(($rlts['error'] ? 'error' : 'success'), $rlts['message'])->with(compact(['is_creation', 'program']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Add children.
     *
     * @param  \App\Http\Requests\MembersAddOrRemoveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function members_add(MembersAddOrRemoveRequest $request, $id)
    {
        $rlts = $this->programRepository->membersAddOrRemove($request, $id);
        return Redirect::to(URL::previous() . "#members")->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }

    /**
     * Add children.
     *
     * @param  \App\Http\Requests\ProgramsAddOrRemoveRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function members_remove(MembersAddOrRemoveRequest $request, $id)
    {
        $rlts = $this->programRepository->membersAddOrRemove($request, $id);
        return Redirect::to(URL::previous() . "#members")->with(($rlts['error'] ? 'error' : 'success'), $rlts['message']);
    }
}
