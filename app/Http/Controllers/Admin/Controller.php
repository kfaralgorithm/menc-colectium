<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Http\Request;
use \App\Interfaces\ProgramRepositoryInterface;
use App\Models\Data;
use App\Models\Program;
use App\Models\Structure;
use App\Models\Unit;

class Controller extends \App\Http\Controllers\Controller
{
    protected $programRepository;
    /**
     * Class constructor.
     */
    public function __construct(ProgramRepositoryInterface $programRepository)
    {
        $this->programRepository = $programRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $programs = $this->programRepository->getList($request)['data'];
        $c_structure = Structure::count();
        $c_program = Program::count();
        $c_unit = Unit::count();
        $c_data = Data::count();
        return view('modules.admin.index')->with(compact(['programs', 'c_structure', 'c_program', 'c_unit', 'c_data',]));
    }
}
