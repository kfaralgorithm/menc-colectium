<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Data;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\DataCreateOrUpdateRequest;
use App\Interfaces\DataRepositoryInterface;
use App\Models\Program;

class DataRepository implements DataRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $data = null;

            $client_available_key = [
                "name" => ['db' => 'name', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';

            $data = Data::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');


            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $data = $data->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $data = $data->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $data = $data->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $data = $data->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $data = $data->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $data = $data->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $data = $data->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $data = $data->get();
            }

            return ['message' => "Recupération de la liste de dataé.", 'data' => $data, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $data = Data::find($id);

            // Check the type
            if (!$data) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $data, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(DataCreateOrUpdateRequest $request, $program_id, $id = null)
    {

        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {

            $program = $program_id ? Program::find($program_id) : new Program;

            if ($id && !$program) return ['message' => "Ressource introuvable $program_id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $data = $id ? Data::find($id) : new Data;

            if ($id && !$data) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];


            if ($request->has('create_or_update_data_name')) {
                $data->name = trim($request->create_or_update_data_name);
            }

            if ($request->has('create_or_update_data_description')) {
                $data->description = trim($request->create_or_update_data_description);
            }

            if ($request->has('create_or_update_data_collect_method')) {
                $data->collect_method = trim($request->create_or_update_data_collect_method);
            }

            if ($request->has('create_or_update_data_source')) {
                $data->source = trim($request->create_or_update_data_source);
            }

            if ($request->has('create_or_update_data_unit')) {
                $data->unit_id = trim($request->create_or_update_data_unit);
            }

            if ($request->has('create_or_update_data_indicator') && $request->create_or_update_data_indicator) {
                $data->indicator_id = trim($request->create_or_update_data_indicator);
            }

            $data->visibility = 1;

            $data->program_id = $program_id;

            if($id){
                if(!$data->responsible_structure_id && auth()->user()->structure){
                    $data->responsible_structure_id = auth()->user()->structure->id;
                }
            }else{
                if(auth()->user()->structure){
                    $data->responsible_structure_id = auth()->user()->structure->id;
                }
            }

            $data->is_enable = $data && !$request->has('create_or_update_data_enable') ? -1 : 1;

            $data->save();

            return [
                'message' => $id ? "Dataé mis à jour." : "Nouveau dataé ajouté.",
                'data' => $data,
                'statusCode' => $id ? 200 : 201,
                'error' => false
            ];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $data = Data::find($id);

            // Check the type
            if (!$data) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $data->name = $data->name . "#-#DELETE#" . time();
            $data->save();

            // Delete the type
            $data->delete();

            DB::commit();
            return ['message' => "Dataé supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
