<?php

namespace App\Repositories;

use App\Models\Program;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Interfaces\ProgramRepositoryInterface;
use App\Http\Requests\MembersAddOrRemoveRequest;
use App\Http\Requests\ProgramCreateOrUpdateRequest;

class ProgramRepository implements ProgramRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $programs = null;

            $client_available_key = [
                "name" => ['db' => 'name', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';

            $programs = Program::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');


            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $programs = $programs->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $programs = $programs->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $programs = $programs->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $programs = $programs->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $programs = $programs->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $programs = $programs->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $programs = $programs->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $programs = $programs->get();
            }

            return ['message' => "Recupération de la liste de programme.", 'data' => $programs, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $program = Program::find($id);

            // Check the type
            if (!$program) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $program, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(ProgramCreateOrUpdateRequest $request, $id = null)
    {

        try {
            $program = $id ? Program::find($id) : new Program;

            if ($id && !$program) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];


            if ($request->has('create_or_update_program_name')) {
                $program->name = trim($request->create_or_update_program_name);
            }

            if ($request->has('create_or_update_program_description')) {
                $program->description = trim($request->create_or_update_program_description);
            }

            if ($request->has('create_or_update_program_objective')) {
                $program->objective = trim($request->create_or_update_program_objective);
            }

            // dd($request->has('create_or_update_program_enable'),  $program && !$request->has('create_or_update_program_enable'));

            $program->is_enable = $program && !$request->has('create_or_update_program_enable') ? -1 : 1;

            $program->save();

            return [
                'message' => $id ? "Programme mis à jour." : "Nouveau programme ajouté.",
                'data' => $program,
                'statusCode' => $id ? 200 : 201,
                'error' => false
            ];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $program = Program::find($id);

            // Check the type
            if (!$program) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $program->name = $program->name . "#-#DELETE#" . time();
            $program->save();

            // Delete the type
            $program->delete();

            DB::commit();
            return ['message' => "Programme supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }


    public function membersAddOrRemove(MembersAddOrRemoveRequest $request, $id)
    {
        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {
            $program = Program::find($id);

            // Check the type
            if (!$program) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            if (isset(request()->_method) && strtoupper(request()->_method) == "DELETE") {
                DB::table('program_structure')
                    ->where('structure_id', $request->members_add_or_remove_program)
                    ->where('program_id', $program->id)
                    ->update(array('deleted_at' => DB::raw('NOW()')));

                return ['message' =>  " 1 structure membre retirée.", 'data' => $program, 'statusCode' => 200, 'error' => false];
            }

            // dd($request->members_add_or_remove_program);

            $program->structures()->attach($request->members_add_or_remove_program);
            return ['message' => count($request->members_add_or_remove_program) . " structure (s) membre (s) ajoutée (s).", 'data' => $program, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
