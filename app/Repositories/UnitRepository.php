<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\Unit;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\UnitCreateOrUpdateRequest;
use App\Interfaces\UnitRepositoryInterface;

class UnitRepository implements UnitRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $units = null;

            $client_available_key = [
                "name" => ['db' => 'name', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';

            $units = Unit::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');


            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $units = $units->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $units = $units->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $units = $units->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $units = $units->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $units = $units->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $units = $units->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $units = $units->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $units = $units->get();
            }

            return ['message' => "Recupération de la liste de unité.", 'data' => $units, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $unit = Unit::find($id);

            // Check the type
            if (!$unit) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $unit, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(UnitCreateOrUpdateRequest $request, $id = null)
    {

        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {
            $unit = $id ? Unit::find($id) : new Unit;

            if ($id && !$unit) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];


            if ($request->has('create_or_update_unit_name')) {
                $unit->name = trim($request->create_or_update_unit_name);
            }

            if ($request->has('create_or_update_unit_symbol')) {
                $unit->symbol = trim($request->create_or_update_unit_symbol);
            }

            if ($request->has('create_or_update_unit_description')) {
                $unit->description = trim($request->create_or_update_unit_description);
            }

            // dd($request->has('create_or_update_unit_enable'),  $unit && !$request->has('create_or_update_unit_enable'));

            $unit->is_enable = $unit && !$request->has('create_or_update_unit_enable') ? -1 : 1;

            $unit->save();

            return [
                'message' => $id ? "Unité mis à jour." : "Nouveau unité ajouté.",
                'data' => $unit,
                'statusCode' => $id ? 200 : 201,
                'error' => false
            ];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $unit = Unit::find($id);

            // Check the type
            if (!$unit) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $unit->name = $unit->name . "#-#DELETE#" . time();
            $unit->save();

            // Delete the type
            $unit->delete();

            DB::commit();
            return ['message' => "Unité supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
