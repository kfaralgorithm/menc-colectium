<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Models\Structure;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Interfaces\StructureRepositoryInterface;
use App\Http\Requests\ProgramsAddOrRemoveRequest;
use App\Http\Requests\StructureCreateOrUpdateRequest;

class StructureRepository implements StructureRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $structures = null;

            $client_available_key = [
                "name" => ['db' => 'name', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';



            $structures = Structure::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');

            $structures = $structures->whereNotIn('name', ['ROOTSADMINISTRATOR']);

            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $structures = $structures->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $structures = $structures->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $structures = $structures->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $structures = $structures->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $structures = $structures->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $structures = $structures->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $structures = $structures->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $structures = $structures->get();
            }

            return ['message' => "Recupération de la liste de structure.", 'data' => $structures, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $structure = Structure::find($id);

            // Check the type
            if (!$structure) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $structure, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(StructureCreateOrUpdateRequest $request, $id = null)
    {
        $structure = null;
        $user = null;
        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {
            $structure = $id ? Structure::find($id) : new Structure;

            if ($id && !$structure) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $old_parent = null;

            if ($id && $request->has('create_or_update_structure_parent')) {

                $old_parent = $structure->parent;
            }

            if ($request->has('create_or_update_structure_name')) {
                $structure->name = trim($request->create_or_update_structure_name);
            }

            if ($request->has('create_or_update_structure_type')) {
                $structure->type_id = $request->create_or_update_structure_type;
            }

            if ($request->has('create_or_update_structure_acronym')) {
                $structure->acronym = $request->create_or_update_structure_acronym;
            }

            // dd($request->has('create_or_update_structure_enable'),  $structure && !$request->has('create_or_update_structure_enable'));

            $structure->is_enable = $structure && !$request->has('create_or_update_structure_enable') ? -1 : 1;

            $structure->save();

            if (!$id || ($structure && !$structure->users()->first() && $request->has('create_or_update_structure_password') && $request->create_or_update_structure_password)) {
                $user = new User;

                $username = null;

                do {
                    $username = "sigds-mnd-" . Str::random(rand(2, 5));
                } while (User::where('username', $username)->count() != 0);

                $user->username = $username;
                $user->password = bcrypt($request->create_or_update_structure_password);
                $user->structure_id = $structure->id;
                $user->save();

                $role = Role::where('name', 'COLLECTEUR')->first();
                DB::table('role_user')->insertGetId(
                    array('id' => Uuid::uuid4()->toString(), 'user_id' => $user->id, 'role_id' => $role->id, 'created_at' => now(), 'updated_at' => now())
                );
            } else {
                $modified = false;
                $user = $structure->users()->first();
                if ($user->username == null || $user->username == 'sigds-mnd' || !(strpos($user->username, 'sigds-mnd') !== false)) {
                    $username = null;
                    do {
                        $username = "sigds-mnd-" . Str::random(rand(2, 5));
                    } while (User::where('username', $username)->count() != 0);

                    $user->username = $username;
                    $modified = true;
                }

                if ($request->has('create_or_update_structure_password') && $request->create_or_update_structure_password) {
                    $user->password = bcrypt($request->create_or_update_structure_password);
                    $modified = true;
                }

                if ($modified) {
                    $user->save();
                }
            }

            return [
                'message' => $id ? "Structure mis à jour." : "Nouveau structure ajouté.",
                'data' => $structure,
                'statusCode' => $id ? 200 : 201,
                'error' => false
            ];
        } catch (\Exception $e) {

            if ($structure && $structure->id) {
                $structure->forceDelete();
            }

            if ($user && $user->id) {
                $user->forceDelete();
            }
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $structure = Structure::find($id);

            // Check the type
            if (!$structure) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $structure->name = $structure->name . "#-#DELETE#" . time();
            $structure->save();

            // Delete the type
            $structure->delete();

            DB::commit();
            return ['message' => "Structure supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }


    public function programsAddOrRemove(ProgramsAddOrRemoveRequest $request, $id)
    {
        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {
            $structure = Structure::find($id);

            // Check the type
            if (!$structure) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            if (isset(request()->_method) && strtoupper(request()->_method) == "DELETE") {

                DB::table('program_structure')
                    ->where('structure_id', $structure->id)
                    ->where('program_id', $request->programs_add_or_remove_from_structure)
                    ->update(array('deleted_at' => DB::raw('NOW()')));


                return ['message' => "1 programme retiré.", 'data' => $structure, 'statusCode' => 200, 'error' => false];
            }

            $programs_affected =   $structure->programs()->attach($request->programs_add_or_remove_from_structure);
            return ['message' => count($request->programs_add_or_remove_from_structure)  . " programme (s) ajouté (s).", 'data' => $structure, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
