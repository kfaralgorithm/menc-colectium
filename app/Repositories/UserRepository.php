<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Interfaces\UserRepositoryInterface;
use App\Http\Requests\ProfilAddOrRemoveRequest;
use App\Http\Requests\UserCreateOrUpdateRequest;
use App\Models\Profil;

class UserRepository implements UserRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $users = null;

            $client_available_key = [
                "firstname" => ['db' => 'firstname', 'type' => 'string'],
                "lastname" => ['db' => 'lastname', 'type' => 'string'],
                "phone" => ['db' => 'phone', 'type' => 'string'],
                "email" => ['db' => 'email', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';

            $users = User::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');


            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $users = $users->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $users = $users->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $users = $users->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $users = $users->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $users = $users->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $users = $users->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $users = $users->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $users = $users->get();
            }

            return ['message' => "Recupération de la liste de useré.", 'data' => $users, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $user = User::find($id);

            // Check the type
            if (!$user) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $user, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(UserCreateOrUpdateRequest $request, $id = null)
    {

        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {
            $user = $id ? User::find($id) : new User;

            if ($id && !$user) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];


            if ($request->has('create_or_update_user_firstname')) {
                $user->firstname = trim($request->create_or_update_user_firstname);
            }

            if ($request->has('create_or_update_user_lastname')) {
                $user->lastname = strtoupper(trim($request->create_or_update_user_lastname));
            }

            if ($request->has('create_or_update_user_email')) {
                $user->email = trim($request->create_or_update_user_email);
            }

            if ($request->has('create_or_update_user_phone')) {
                $user->phone = strtolower(trim($request->create_or_update_user_phone));
            }

            if (!$id) {
                $random = generate_strong_password(rand(8, 10));
                $user->password = bcrypt($random);
            }

            $user->is_enable = $user && !$request->has('create_or_update_user_enable') ? -1 : 1;

            $user->save();

            return [
                'message' => $id ? "Utilisateur mis à jour." : "Nouveau utilisateur ajouté.",
                'data' => $user,
                'statusCode' => $id ? 200 : 201,
                'error' => false
            ];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $user = User::find($id);

            // Check the type
            if (!$user) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $user->name = $user->name . "#-#DELETE#" . time();
            $user->save();

            // Delete the type
            $user->delete();

            DB::commit();
            return ['message' => "Useré supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }


    public function profilAddOrRemove(ProfilAddOrRemoveRequest $request, $id)
    {
        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {
            $user = User::find($id);

            // Check the type
            if (!$user) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];


            $profil = Profil::where('structure_id', $request->profil_add_or_remove_user)->first();

            if (isset(request()->_method) && strtoupper(request()->_method) == "DELETE") {
                $profil->update([
                    'is_enable' => 0,
                    'disable_blocked_at' => \Carbon\Carbon::now()
                ]);

                return ['message' =>  "Profil associé à " . $profil->structure->name . " désactivé.", 'data' => $user, 'statusCode' => 200, 'error' => false];
            }

            // dd($request->profil_add_or_remove_user);

            if (!$profil) {
                $profil = Profil::create([
                    'is_enable' => 1,
                    'structure_id' => $request->profil_add_or_remove_user,
                    'owner_id' => $id,
                    'children_structure_autorised' => $request->has('create_or_update_structure_acronym') ? true : false,
                ]);
            }
            $profil->update([
                'is_enable' => 1,
                'disable_blocked_at' => null,
                'children_structure_autorised' => $request->has('create_or_update_structure_acronym') ? true : false,
            ]);

            Profil::where('owner_id', $id)
                ->whereNotIn('id', array($profil->id))->update(array('is_enable' => 0, 'disable_blocked_at' => \Carbon\Carbon::now()));


            return ['message' => "Profil activé pour la structure " . $profil->structure->name . ".", 'data' => $user, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
