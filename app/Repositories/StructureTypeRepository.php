<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Models\StructureType;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\StructureTypeCreateOrUpdateRequest;
use App\Interfaces\StructureTypeRepositoryInterface;

class StructureTypeRepository implements StructureTypeRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $types = null;

            $client_available_key = [
                "name" => ['db' => 'name', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';

            $types = StructureType::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');


            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $types = $types->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $types = $types->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $types = $types->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $types = $types->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $types = $types->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $types = $types->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $types = $types->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $types = $types->get();
            }

            return ['message' => "Recupération de la liste de type de structure.", 'data' => $types, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $type = StructureType::find($id);

            // Check the type
            if (!$type) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $type, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(StructureTypeCreateOrUpdateRequest $request, $id = null)
    {
        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];


        try {
            $type = $id ? StructureType::find($id) : new StructureType;

            if ($id && !$type) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];


            if ($request->has('create_or_update_type_structure_name')) {
                $type->name = trim($request->create_or_update_type_structure_name);
            }

            if ($request->has('create_or_update_type_structure_description')) {
                $type->description = trim($request->create_or_update_type_structure_description);
            }

            // dd($request->has('create_or_update_type_structure_enable'),  $type && !$request->has('create_or_update_type_structure_enable'));

            $type->is_enable = $type && !$request->has('create_or_update_type_structure_enable') ? -1 : 1;

            $type->save();

            return [
                'message' => $id ? "Type de structure mis à jour." : "Nouveau type de structure ajouté.",
                'data' => $type,
                'statusCode' => $id ? 200 : 201,
                'error' => false
            ];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $type = StructureType::find($id);

            // Check the type
            if (!$type) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $type->name = $type->name . "#-#DELETE#" . time();
            $type->save();

            // Delete the type
            $type->delete();

            DB::commit();
            return ['message' => "Type de structure supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
