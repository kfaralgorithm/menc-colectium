<?php

namespace App\Repositories;

use App\Models\Data;
use App\Models\Record;
use App\Models\Program;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Interfaces\RecordRepositoryInterface;
use App\Http\Requests\RecordCreateOrUpdateRequest;

class RecordRepository implements RecordRepositoryInterface
{


    public function getList(Request $request, bool $is_paginated = false, int $default_line = 25)
    {
        try {

            $records = null;

            $client_available_key = [
                "name" => ['db' => 'name', 'type' => 'string'],
                "is_enable" => ['db' => 'is_enable', 'type' => 'numeric'],
                "created_at" =>  ['db' => 'created_at', 'type' => 'datetime'],
            ];

            $sort_by_available_key = array_merge(array_keys($client_available_key), preg_filter('/^/', '-', array_keys($client_available_key)));


            /*
             * Sort scope field
             */
            $client_sort  = $request->has('sort_by') && in_array($request->sort_by, $sort_by_available_key) ? $request->has('sort_by') : 'created_at';

            $records = Record::orderBy($client_sort, $client_sort[0] == '-' ? 'asc' : 'desc');


            foreach ($client_available_key as $key => $value) {

                if ($request->has($key) && $request->$key && $value['type'] == 'string') {
                    /*
                     * Filtring on string client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $records = $records->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], 'LIKE', '%' . trim($request->$key) . '%');
                        });
                    } else {
                        $records = $records->where($value['db'], 'LIKE', '%' . trim($request->$key) . '%');
                    }
                } else if ($request->has('before_' . $key) && $request->{'before_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {

                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */


                    if (strpos($value['db'], ".") !== false) {
                        $records = $records->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '<=', $request->{'before_' . $key});
                        });
                    } else {
                        $records = $records->where($value['db'], '<=', $request->{'before_' . $key});
                    }
                } else if ($request->has('after_' . $key) && $request->{'after_' . $key} && in_array($value['type'], ['numeric', 'date', 'datetime'])) {
                    /*
                     * Filtring on ['numeric', 'date', 'datetime'] client_available_key field
                     */

                    if (strpos($value['db'], ".") !== false) {
                        $records = $records->whereHas(explode('.', $value['db'])[0], function (Builder $query) use ($value, $request, $key) {
                            $query->where(explode('.', $value['db'])[1], '>=', $request->{'after_' . $key});
                        });
                    } else {
                        $records = $records->where($value['db'], '>=', $request->{'after_' . $key});
                    }
                }
            }

            if ($is_paginated) {
                $records = $records->paginate($default_line)->appends(Arr::except($request->query(), 'page'));
            } else {
                $records = $records->get();
            }

            return ['message' => "Recupération de la liste de dataé.", 'data' => $records, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => collect(), 'statusCode' => 500, 'error' => true];
        }
    }


    public function getById($id)
    {

        try {
            $record = Record::find($id);

            // Check the type
            if (!$record) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            return ['message' => "Structure type Detail", 'data' => $record, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function createOrUpdate(RecordCreateOrUpdateRequest $request, $data_id)
    {

        if (isset($request->validator) && $request->validator->fails()) return ['message' => implode(' ', $request->validator->errors()->all()), 'data' => null, 'statusCode' => 400, 'error' => true];

        try {

            $data = Data::find($data_id);

            if (!$data) return ['message' => "Ressource introuvable $data_id", 'data' => null, 'statusCode' => 404, 'error' => true];


            $add = 0;
            $update = 0;

            foreach (range(1, 4) as $index) {
                $field_name = 'data_record_' . $index;

                $record = Record::where('data_id', $data_id)->where('trimestre_number', $index)->first();

                $instanced = false;

                if ($request->has($field_name) && $request->$field_name) {
                    if ($record && $record->value != trim($request->$field_name)) {
                        $update++;
                        $instanced = true;
                    } elseif (!$record) {
                        $add++;
                        $instanced = true;
                        $record = new Record;
                    }

                    if ($instanced) {
                        $record->value = trim($request->$field_name);
                        $record->visibility = 1;
                        $record->trimestre_number = $index;
                        $record->data_id = $data_id;
                        $record->year = date("Y");
                        $record->save();
                    }
                }
            }


            return [
                'message' => $add . " nouveau (x) enregistrement (s). " . $update . " enregistrement (s) modifié (s)",
                'data' => $data,
                'statusCode' => 200,
                'error' => false
            ];
        } catch (\Exception $e) {
            return ['message' => json_encode($e->getMessage()), 'data' => null, 'statusCode' => 500, 'error' => true];
        }
    }


    public function delete($id)
    {

        DB::beginTransaction();
        try {
            $record = Record::find($id);

            // Check the type
            if (!$record) return ['message' => "Ressource introuvable $id", 'data' => null, 'statusCode' => 404, 'error' => true];

            $record->name = $record->name . "#-#DELETE#" . time();
            $record->save();

            // Delete the type
            $record->delete();

            DB::commit();
            return ['message' => "Dataé supprimé.", 'data' => true, 'statusCode' => 200, 'error' => false];
        } catch (\Exception $e) {
            DB::rollBack();
            return ['message' => json_encode($e->getMessage()), 'data' => false, 'statusCode' => 500, 'error' => true];
        }
    }
}
