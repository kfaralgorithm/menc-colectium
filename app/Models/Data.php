<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Sqits\UserStamps\Concerns\HasUserStamps;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Data extends Model
{
    use HasFactory, HasUserStamps, SoftDeletes, Uuids;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "data";

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'is_enable',
        'description',
        'collect_method',
        'source',
        'unit_id',
        'indicator_id',
        'program_id',
        'visibility',
        'responsible_structure_id'
    ];

    /**
     * Get the unit that owns the Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class, 'unit_id');
    }



    /**
     * Get the indicator that owns the Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function indicator()
    {
        return $this->belongsTo(Indicator::class, 'indicator_id');
    }


    /**
     * Get the responsible_structure that owns the Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsible_structure()
    {
        return $this->belongsTo(Structure::class, 'responsible_structure_id');
    }

    /**
     * Get all of the records for the Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function records(): HasMany
    {
        return $this->hasMany(Record::class, 'data_id', 'id');
    }

    /**
     * Get the program that owns the Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function program(): BelongsTo
    {
        return $this->belongsTo(Program::class, 'program_id');
    }
}
