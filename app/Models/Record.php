<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Sqits\UserStamps\Concerns\HasUserStamps;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Record extends Model
{
    use HasFactory, HasUserStamps, SoftDeletes, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'value',
        'year',
        'trimestre_number',
        'data_id',
        'visibility',
    ];

    /**
     * Get the data that owns the Record
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function data(): BelongsTo
    {
        return $this->belongsTo(Data::class, 'data_id');
    }
}
