<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Sqits\UserStamps\Concerns\HasUserStamps;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Structure extends Model
{
    use HasFactory, HasUserStamps, SoftDeletes, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'acronym',
        'is_enable',
        'type_id',
        'hierarchy'
    ];

    /**
     * Get the type that owns the Structure
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(StructureType::class, 'type_id');
    }

    /**
     * The programs that belong to the Structure
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function programs()
    {
        return $this
            ->belongsToMany(Program::class)
            ->whereNull('program_structure.deleted_at') // Table `program_structure` has column `deleted_at`
            ->withTimestamps(); // Table `program_structure` has columns: `created_at`, `updated_at`

    }

    /**
     * Get all of the children for the Structure
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Structure::class, 'parent_id');
    }

    /**
     * Get all of the data for the Unit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function data()
    {
        return $this->hasMany(Data::class, 'responsible_structure_id');
    }

    /**
     * Get all of the users for the Structure
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'structure_id');
    }
}
