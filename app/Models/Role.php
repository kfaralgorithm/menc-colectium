<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_disabled'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user')->withTimestamps()->withPivot('is_current');
    }
}
