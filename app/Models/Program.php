<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Sqits\UserStamps\Concerns\HasUserStamps;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Program extends Model
{
    use HasFactory, HasUserStamps, SoftDeletes, Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'objective',
        'name',
        'is_enable',
        'description',
    ];

    public function structures()
    {

        return $this
            ->belongsToMany(Structure::class)
            ->whereNull('program_structure.deleted_at') // Table `program_structure` has column `deleted_at`
            ->withTimestamps(); // Table `program_structure` has columns: `created_at`, `updated_at`

    }

    /**
     * Get all of the data for the Program
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function data(): HasMany
    {
        return $this->hasMany(Data::class, 'program_id', 'id');
    }

    /**
     * Get all of the records for the Program
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function records(): HasManyThrough
    {
        return $this->hasManyThrough(Record::class, Data::class);
    }
}
