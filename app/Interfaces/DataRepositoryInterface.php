<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\DataCreateOrUpdateRequest;

interface DataRepositoryInterface
{
    /**
     * Get all Structures
     *
     * @access  public
     */
    public function getList(Request $request);

    /**
     * Get Structure By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update Structure
     *
     * @param   \App\Http\Requests\DataCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $program_id
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(DataCreateOrUpdateRequest $request, $program_id, $id = null);

    /**
     * Delete Structure
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
