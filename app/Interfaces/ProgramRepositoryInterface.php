<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\MembersAddOrRemoveRequest;
use App\Http\Requests\ProgramCreateOrUpdateRequest;

interface ProgramRepositoryInterface
{
    /**
     * Get all Structures
     *
     * @access  public
     */
    public function getList(Request $request);

    /**
     * Get Structure By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update Structure
     *
     * @param   \App\Http\Requests\ProgramCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(ProgramCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete Structure
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);


    /**
     * Add children Structure
     *
     * @param   \App\Http\Requests\MembersAddOrRemoveRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function membersAddOrRemove(MembersAddOrRemoveRequest $request, $id);
}
