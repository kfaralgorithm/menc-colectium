<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\UnitCreateOrUpdateRequest;

interface UnitRepositoryInterface
{
    /**
     * Get all Structures
     *
     * @access  public
     */
    public function getList(Request $request);

    /**
     * Get Structure By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update Structure
     *
     * @param   \App\Http\Requests\UnitCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(UnitCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete Structure
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);
}
