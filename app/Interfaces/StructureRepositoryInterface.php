<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\ProgramsAddOrRemoveRequest;
use App\Http\Requests\StructureCreateOrUpdateRequest;

interface StructureRepositoryInterface
{
    /**
     * Get all Structures
     *
     * @access  public
     */
    public function getList(Request $request);

    /**
     * Get Structure By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update Structure
     *
     * @param   \App\Http\Requests\StructureCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(StructureCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete Structure
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);


    /**
     * Add programs Structure
     *
     * @param   \App\Http\Requests\ProgramsAddOrRemoveRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function programsAddOrRemove(ProgramsAddOrRemoveRequest $request, $id);
}
