<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use App\Http\Requests\ProfilAddOrRemoveRequest;
use App\Http\Requests\UserCreateOrUpdateRequest;

interface UserRepositoryInterface
{
    /**
     * Get all Structures
     *
     * @access  public
     */
    public function getList(Request $request);

    /**
     * Get Structure By ID
     *
     * @param   \Ramsey\Uuid\Uuid      $id
     *
     * @access  public
     */
    public function getById($id);

    /**
     * Create | Update Structure
     *
     * @param   \App\Http\Requests\UserCreateOrUpdateRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function createOrUpdate(UserCreateOrUpdateRequest $request, $id = null);

    /**
     * Delete Structure
     *
     * @param   \Ramsey\Uuid\Uuid     $id
     *
     * @access  public
     */
    public function delete($id);


    /**
     * Add profil
     *
     * @param   \App\Http\Requests\ProfilAddOrRemoveRequest    $request
     * @param   \Ramsey\Uuid\Uuid                           $id
     *
     * @access  public
     */
    public function profilAddOrRemove(ProfilAddOrRemoveRequest $request, $id);
}
