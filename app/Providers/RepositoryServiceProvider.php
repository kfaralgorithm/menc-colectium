<?php

namespace App\Providers;

use App\Repositories\DataRepository;
use App\Repositories\UnitRepository;
use App\Repositories\UserRepository;
use App\Repositories\RecordRepository;
use App\Repositories\ProgramRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\IndicatorRepository;
use App\Repositories\StructureRepository;
use App\Interfaces\DataRepositoryInterface;
use App\Interfaces\UnitRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\RecordRepositoryInterface;
use App\Repositories\StructureTypeRepository;
use App\Interfaces\ProgramRepositoryInterface;
use App\Interfaces\IndicatorRepositoryInterface;
use App\Interfaces\StructureRepositoryInterface;
use App\Interfaces\StructureTypeRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StructureTypeRepositoryInterface::class, StructureTypeRepository::class);
        $this->app->bind(StructureRepositoryInterface::class, StructureRepository::class);
        $this->app->bind(IndicatorRepositoryInterface::class, IndicatorRepository::class);
        $this->app->bind(UnitRepositoryInterface::class, UnitRepository::class);
        $this->app->bind(ProgramRepositoryInterface::class, ProgramRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(DataRepositoryInterface::class, DataRepository::class);
        $this->app->bind(RecordRepositoryInterface::class, RecordRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
