<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Admin\Controller as AdminController;
use App\Http\Controllers\Admin\UnitController as AdminUnitController;
use App\Http\Controllers\Admin\ProgramController as AdminProgramController;
use App\Http\Controllers\Admin\IndicatorController as AdminIndicatorController;
use App\Http\Controllers\Admin\StructureController as AdminStructureController;
use App\Http\Controllers\Collectors\DataController as CollectorsDataController;
use App\Http\Controllers\Collectors\RecordController as CollectorsRecordController;
use App\Http\Controllers\Collectors\ProgramController as CollectorsProgramController;
use App\Http\Controllers\Admin\StructureTypeController as AdminStructureTypeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'revalidate_back_history'], function () {

    Route::group(['as' => 'auth.', 'prefix' => 'auth'], function () {

        Route::get('/', [LoginController::class, 'create'])->name('login');
        Route::get('/logout', [LogoutController::class, 'logout'])->name('logout');



        Route::group(['middleware' => ['costum_guest']], function () {
            Route::post('/', [LoginController::class, 'store'])->name('login.store');


            Route::get('/forgot-password', function () {
                return view('modules.shared.auth.login');
            });

            Route::get('/reset-password', function () {
                return view('modules.shared.auth.login');
            });
        });
    });


    Route::get('/', [HomeController::class, 'default'])->name('home.default');

    Route::group(['middleware' => 'administrator_checker', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/', [AdminController::class, 'index'])->name('index');

        Route::resource('structures', AdminStructureController::class);
        Route::group(['prefix' => 'structures', 'as' => 'structures.'], function () {
            Route::post('/{structure}/programs', [AdminStructureController::class, 'programs_add'])->name('programs.add');
            Route::delete('/{structure}/programs', [AdminStructureController::class, 'programs_remove'])->name('programs.remove');
        });

        Route::resource('structure-types', AdminStructureTypeController::class);
        Route::resource('units', AdminUnitController::class);
        Route::resource('indicators', AdminIndicatorController::class);

        Route::resource('programs', AdminProgramController::class);
        Route::group(['prefix' => 'programs', 'as' => 'programs.'], function () {
            Route::post('/{program}/structures', [AdminProgramController::class, 'members_add'])->name('members.add');
            Route::delete('/{program}/structures', [AdminProgramController::class, 'members_remove'])->name('members.remove');
        });
    });


    Route::group(['middleware' => 'collector_checker', 'prefix' => 'pf', 'as' => 'pf.'], function () {
        Route::get('/', [CollectorsProgramController::class, 'index'])->name('collectors.index');


        Route::group(['prefix' => '/{program}', 'as' => 'programs.'], function () {
            Route::resource('data', CollectorsDataController::class)->except(['show']);

            Route::get('/data/with-values', [CollectorsDataController::class, 'index_with_values'])->name('data.index.with.values');


            Route::group(['prefix' => '/data/{data}', 'as' => 'data.'], function () {
                Route::resource('records', CollectorsRecordController::class)->only(['index', 'create', 'store']);
            });
        });
    });
});
