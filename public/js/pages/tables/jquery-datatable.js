$(function () {
    //Exportable table
    $(".js-exportable").DataTable({
        dom: "Bfrtip",
        responsive: true,
        buttons: ["copy", "csv", "excel", "pdf", "print"],
        language: {
            emptyTable: "Aucune donnée disponible",
            search: "Recherche:",
            info: "Page _START_ avec _END_ sur _TOTAL_ élément(s)",
            paginate: {
                previous: '<i class="material-icons">navigate_before</i>',
                next: '<i class="material-icons">navigate_next</i>',
            },
        },
        order: [[0, "desc"]],
    });
});
