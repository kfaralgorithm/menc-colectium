$(function () {
    //Exportable table
    $(".js-exportable").DataTable({
        dom: "Bfrtip",
        responsive: true,
        //buttons: ["copy", "csv", "excel", "pdf", "print"],
        buttons: [
            {
                extend: "copy",
                exportOptions: {
                    columns: [".export"],
                },
            },
            {
                extend: "excel",
                exportOptions: {
                    columns: [".export"],
                },
            },
            {
                extend: "csv",
                exportOptions: {
                    columns: [".export"],
                },
            },
            {
                extend: "print",
                exportOptions: {
                    columns: [".export"],
                },
            },
            {
                extend: "pdf",
                exportOptions: {
                    columns: [".export"],
                },
            },
            //'copy', 'excel', 'csv', 'print' // , 'pdf' - Wait for next PDFMake release because of this bug https://github.com/bpampuch/pdfmake/pull/443
        ],
        language: {
            emptyTable: "Aucune donnée disponible",
            search: "Recherche:",
            info: "Page _START_ avec _END_ sur _TOTAL_ élément(s)",
            paginate: {
                previous: '<i class="material-icons">navigate_before</i>',
                next: '<i class="material-icons">navigate_next</i>',
            },
        },
        order: [],
    });
});
