$(function () {
    //Vertical form basic
    $("#wizard_vertical").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        stepsOrientation: "vertical",
        onInit: function (event, currentIndex) {
            setButtonWavesEffect(event);
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinished: function (event, currentIndex) {
            if ($("#wizard-form").length) {
                console.log("End ++++++++++++");
                $("#wizard-form").submit();
            }
        },
        saveState: true,
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget)
        .find('[role="menu"] li a')
        .removeClass("waves-effect");
    $(event.currentTarget)
        .find('[role="menu"] li:not(.disabled) a')
        .addClass("waves-effect");
}
