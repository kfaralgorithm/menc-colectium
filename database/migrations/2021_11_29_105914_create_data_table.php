<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name')->unique();
            $table->enum('is_enable', [1, -1])->default(1);
            $table->text('collect_method');
            $table->text('source');
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->uuid('unit_id')->index();
            $table->foreign('unit_id')->references('id')->on('units');
            $table->uuid('responsible_structure_id')->index()->nullable();
            $table->foreign('responsible_structure_id')->references('id')->on('structures');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
