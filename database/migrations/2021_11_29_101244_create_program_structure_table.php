<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramStructureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_structure', function (Blueprint $table) {
            $table->uuid('structure_id')->index()->nullable();
            $table->foreign('structure_id')->references('id')->on('structures');
            $table->uuid('program_id')->index()->nullable();
            $table->foreign('program_id')->references('id')->on('programs');
            $table->id();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_structure');
    }
}
