<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColletorSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::doesnthave('roles')->get();

        foreach ($users as $user) {
            if ($user->username && strpos($user->username, 'sigds-mnd') !== false) {
                $role = Role::where('name', 'COLLECTEUR')->first();
                DB::table('role_user')->insertGetId(
                    array('id' => Uuid::uuid4()->toString(), 'user_id' => $user->id, 'role_id' => $role->id, 'created_at' => now(), 'updated_at' => now())
                );
            }
        }
    }
}
