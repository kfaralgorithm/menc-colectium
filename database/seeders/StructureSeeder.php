<?php

namespace Database\Seeders;

use App\Models\Structure;
use Illuminate\Database\Seeder;

class StructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $structures = [
            'ROOTSADMINISTRATOR' => ['is_enable' => 1],
        ];
        foreach ($structures as $name => $value) {
            $structure = Structure::where('name', $name)->first();
            if ($structure) {
                $structure->is_enable = $value['is_enable'];
                $structure->save();
            } else {
                Structure::create([
                    'name' => $name,
                    'is_enable' => $value['is_enable'],
                    "created_at" => \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now(),
                ]);
            }
        }
    }
}
