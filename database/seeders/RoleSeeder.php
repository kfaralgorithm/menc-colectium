<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'COLLECTEUR' => ['is_disabled' => false],
            'ADMINISTRATEUR' => ['is_disabled' => false],
        ];
        foreach ($roles as $name => $value) {
            $role = Role::where('name', $name)->first();
            if ($role) {
                $role->is_disabled = $value['is_disabled'];
                $role->save();
            } else {
                Role::create([
                    'name' => $name,
                    'is_disabled' => $value['is_disabled'],
                    "created_at" => \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now(),
                ]);
            }
        }
    }
}
