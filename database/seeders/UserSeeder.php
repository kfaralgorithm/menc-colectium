<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Ramsey\Uuid\Uuid;
use App\Models\Structure;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 5; $i++) {
            $username = "administrateur-" . $i;
            $user =  User::where('username', $username)->first();
            $structure = Structure::where('name', 'ROOTSADMINISTRATOR')->first();
            if ($user) {
                $user->structure_id = $structure->id;
                $user->save();
            } else {
                $user = User::create([
                    'username' => $username,
                    'password' => bcrypt("ROOTx" . $i . "#2021a"),
                    "structure_id" => $structure->id,
                    "created_at" => \Carbon\Carbon::now(),
                    "updated_at" => \Carbon\Carbon::now(),
                ]);

                DB::table('role_user')->insertGetId(
                    array('id' => Uuid::uuid4()->toString(), 'user_id' => $user->id, 'role_id' => Role::where('name', 'ADMINISTRATEUR')->first()->id, 'created_at' => now(), 'updated_at' => now())
                );
            }
        }
    }
}
